package com.myqnapcloud.mkokos.domain;//package com.myqnapcloud.mkokos.buttons.data;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

@Entity
@Table(name = "deviceID_tbl")

public class DeviceID {

    @Id
    @GeneratedValue(
            strategy= GenerationType.AUTO,
            generator="native"
    )
    @GenericGenerator(
            name = "native",
            strategy = "native"
    )

    private Long id;
    @Column(unique = true,nullable = true)
    private String mac;
    private String shortName;
    private String description;
    private DeviceType type;
    private boolean active;
    @ManyToOne(cascade = CascadeType.DETACH)
    private User user;
    @ManyToOne(cascade = CascadeType.REMOVE)
    private DeviceGroup deviceGroup;


    public String getMac() {
        return mac;
    }

    public void setMac(String mac) {
        this.mac = mac;
    }

    public DeviceGroup getDeviceGroup() {
        return deviceGroup;
    }

    public void setDeviceGroup(DeviceGroup deviceGroup) {
        this.deviceGroup = deviceGroup;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public DeviceID(String shortName, String description, DeviceType type, User user) {
        this.shortName = shortName;
        this.description = description;
        this.type = type;
        this.user = user;
        this.active=true;
        this.deviceGroup =null;
    }
    public DeviceID(String shortName, String description, DeviceType type, User user, DeviceGroup deviceGroup) {
        this.shortName = shortName;
        this.description = description;
        this.type = type;
        this.user = user;
        this.active=true;
        this.deviceGroup = deviceGroup;
    }
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public DeviceType getType() {
        return type;
    }

    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    public void setType(DeviceType type) {
        this.type = type;
    }

    public DeviceID() {
    }

    @Override
    public String toString() {
        return "DeviceID{" +
                "id=" + id +
                ", description='" + description + '\'' +
                ", type=" + type +
                ", user=" + user +
                "}";
    }
}