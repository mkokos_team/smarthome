package com.myqnapcloud.mkokos.domain;


public class ControlForm {
    private String shortName;
    private String description;
    private String controltype;
    private String buttontype;
    private Integer slidermin;
    private Integer slidermax;
    private Integer sliderval;
    private String buttonstate;
    private Integer buttontimer;
    private String deviceIDRELAY;
    private String deviceIDTEMPERATURE;
    private String deviceIDSTEPER;


    public ControlForm() {
    }

    public Integer getButtontimer() {
        return buttontimer;
    }

    public void setButtontimer(Integer buttontimer) {
        this.buttontimer = buttontimer;
    }

    public String getDeviceIDRELAY() {
        return deviceIDRELAY;
    }

    public void setDeviceIDRELAY(String deviceIDRELAY) {
        this.deviceIDRELAY = deviceIDRELAY;
    }

    public String getDeviceIDTEMPERATURE() {
        return deviceIDTEMPERATURE;
    }

    public void setDeviceIDTEMPERATURE(String deviceIDTEMPERATURE) {
        this.deviceIDTEMPERATURE = deviceIDTEMPERATURE;
    }

    public String getDeviceIDSTEPER() {
        return deviceIDSTEPER;
    }

    public void setDeviceIDSTEPER(String deviceIDSTEPER) {
        this.deviceIDSTEPER = deviceIDSTEPER;
    }

    public String getButtonstate() {
        return buttonstate;
    }

    public void setButtonstate(String buttonstate) {
        this.buttonstate = buttonstate;
    }

    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getControltype() {
        return controltype;
    }

    public void setControltype(String controltype) {
        this.controltype = controltype;
    }

    public String getButtontype() {
        return buttontype;
    }

    public void setButtontype(String buttontype) {
        this.buttontype = buttontype;
    }

    public Integer getSlidermin() {
        return slidermin;
    }

    public void setSlidermin(Integer slidermin) {
        this.slidermin = slidermin;
    }

    public Integer getSlidermax() {
        return slidermax;
    }

    public void setSlidermax(Integer slidermax) {
        this.slidermax = slidermax;
    }

    public Integer getSliderval() {
        return sliderval;
    }

    public void setSliderval(Integer sliderval) {
        this.sliderval = sliderval;
    }
}
