package com.myqnapcloud.mkokos.domain;


import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

@Entity
@Table(name = "States_tbl")
public class State {
    @Id
    @GeneratedValue(
            strategy= GenerationType.AUTO,
            generator="native"
    )
    @GenericGenerator(
            name = "native",
            strategy = "native"
    )
    private Long id;
    private Boolean boolState;
    private Float floatState;
    private Integer intState;
    private Integer intMaxState;
    private Integer intMinState;

    public State() {

        intMinState = 0;
        intMaxState = 100;
    }

    public State(Boolean boolState) {
        this.boolState = boolState;
        intMinState = 0;
        intMaxState = 100;
    }

    public State(Float floatState) {
        this.floatState = floatState;
        intMinState = 0;
        intMaxState = 100;
    }

    public State(Integer intState) {
        this.intState = intState;
        intMinState = 0;
        intMaxState = 100;
    }
    public State(Integer intState,Integer intMinState,Integer intMaxState) {
        this.intMinState=intMinState;
        this.intMaxState=intMaxState;
        this.intState = isIntInRange(intState);
    }
    private Integer isIntInRange(Integer intState){
        if(intState>this.intMaxState)
            return this.intMaxState;
        else
            if (intState<this.intMinState)
                return this.intMinState;
            else
                return intState;
    }

    Boolean getBoolState() {
        return boolState;
    }

    void setBoolState(Boolean boolState) {
        this.boolState = boolState;
    }

    Float getFloatState() {
        return floatState;
    }

    void setFloatState(Float floatState) {
        this.floatState = floatState;
    }

    Integer getIntState() {
        return intState;
    }

    void setIntState(Integer intState) {

        this.intState = intState;
    }

    public Integer getIntMaxState() {
        return intMaxState;
    }

    public void setIntMaxState(Integer intMaxState) {
        this.intMaxState = intMaxState;
    }

    public Integer getIntMinState() {
        return intMinState;
    }

    public void setIntMinState(Integer intMinState) {
        this.intMinState = intMinState;
    }

    @Override
    public String toString() {
        return "State{" +
                "id=" + id +
                ", boolState=" + boolState +
                ", floatState=" + floatState +
                ", intState=" + intState +
                ", intMaxState=" + intMaxState +
                ", intMinState=" + intMinState +
                '}';
    }
}
