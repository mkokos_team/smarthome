package com.myqnapcloud.mkokos.domain;//package com.myqnapcloud.mkokos.buttons.data;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "devicestates_tbl")
public class DeviceState {

    @Id
    @GeneratedValue(
            strategy= GenerationType.AUTO,
            generator="native"
    )
    @GenericGenerator(
            name = "native",
            strategy = "native"
    )
    private Long id;
    private LocalDateTime dateTime;
    @OneToOne (cascade = CascadeType.REMOVE)
    private State state;
    @ManyToOne (cascade = CascadeType.DETACH)
   private DeviceID deviceID;

    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
    }

    public DeviceState() {
    }
    public DeviceState(State state,DeviceID deviceID) {
        this.dateTime = LocalDateTime.now();
        this.state = state;
        this.deviceID = deviceID;
    }
    public DeviceState(boolean state, DeviceID deviceID) {
        this.dateTime = LocalDateTime.now();
        this.state = new State(state);
        this.deviceID = deviceID;
    }
    public DeviceState(float state, DeviceID deviceID) {
        this.dateTime = LocalDateTime.now();
        this.state = new State(state);
        this.deviceID = deviceID;
    }
    public DeviceState(Integer state, Integer minState, Integer maxState, DeviceID deviceID) {
        this.dateTime = LocalDateTime.now();
        this.state = new State(state,minState,maxState);
        this.deviceID = deviceID;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDateTime getDateTime() {
        return dateTime;
    }

    public void setDateTime(LocalDateTime dateTime) {
        this.dateTime = dateTime;
    }
    public Integer getSliderState(){
        return state.getIntState();
    }

    public Float getTemperature(){
        return state.getFloatState();
    }
    public boolean isButtonState() {
        return state.getBoolState();
    }
    public void setTemperature(Float state){
        this.state.setFloatState(state);
    }
    public void setSliderState(Integer state){
        this.state.setIntState(state);
    }
    public void setButtonState(boolean buttonState) {
        this.state.setBoolState(buttonState);
    }
    public String getBoolState(){
        return ""+this.state.getBoolState();
    }
    public String getFloatState(){
        return ""+this.state.getFloatState();
    }

    public String getIntState(){
        return ""+this.state.getIntState();
    }
    public DeviceID getDeviceID() {
        return deviceID;
    }

    public void setDeviceID(DeviceID deviceID) {
        this.deviceID = deviceID;
    }

    @Override
    public String toString() {
        return "DeviceState{" +
                "id=" + id +
                ", dateTime=" + dateTime +
                ", state=" + state +
                ", deviceID=" + deviceID +
                '}';
    }
}