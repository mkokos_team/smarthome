package com.myqnapcloud.mkokos.domain;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.io.UnsupportedEncodingException;
import java.util.Set;
import java.util.UUID;

@Entity
@Table(name = "user")
public class User {
    @Id
    @GeneratedValue(
            strategy= GenerationType.AUTO,
            generator="native"
    )
    @GenericGenerator(
            name = "native",
            strategy = "native"
    )
    private Long id;
//    @UniqueElements
    private String username;

    private String password;

    @Transient
    private String passwordConfirm;

    private String token;

    @ManyToMany
    private Set<Role> roles;
    @OneToMany(cascade = CascadeType.ALL,mappedBy = "user")
    private Set<ControlID> controlIDS;

    public void generateToken() throws UnsupportedEncodingException {
        byte[] bytes = this.username.getBytes("UTF-8");
        this.token = UUID.nameUUIDFromBytes(bytes).toString();
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPasswordConfirm() {
        return passwordConfirm;
    }

    public void setPasswordConfirm(String passwordConfirm) {
        this.passwordConfirm = passwordConfirm;
    }

    public Set<Role> getRoles() {
        return roles;
    }

    public void setRoles(Set<Role> roles) {
        this.roles = roles;
    }
}
