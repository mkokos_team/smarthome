package com.myqnapcloud.mkokos.domain;//package com.myqnapcloud.mkokos.buttons.data;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

@Entity
@Table(name = "controlID_tbl")

public class ControlID {

    @Id
    @GeneratedValue(
            strategy = GenerationType.AUTO,
            generator = "native"
    )
    @GenericGenerator(
            name = "native",
            strategy = "native"
    )

    private Long id;
    private String shortName;
    private String description;
    private ControlType type;
    @ManyToOne(cascade = CascadeType.DETACH)
    private DeviceID deviceID;
    private Integer timer;
    private Integer positionOfIcon;
    @ManyToOne(cascade = CascadeType.DETACH)
    private User user;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    public DeviceID getDeviceID() {
        return deviceID;
    }

    public void setDeviceID(DeviceID deviceID) {
        this.deviceID = deviceID;
    }

    public void setPositionOfIcon(Integer positionOfIcon) {
        this.positionOfIcon = positionOfIcon;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public ControlID() {
    }

    public Integer getPositionOfIcon() {
        return positionOfIcon;
    }

    public Integer getTimer() {
        return timer;
    }

    public void setTimer(Integer timer) {
        this.timer = timer;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public ControlType getType() {
        return type;
    }

    public void setType(ControlType type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "ControlID{" +
                "id=" + id +
                ", name='" + shortName +
                ", description='" + description + '\'' +
                ", type=" + type +
                ", user=" + user +
                '}';
    }
}