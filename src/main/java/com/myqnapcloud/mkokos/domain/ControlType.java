package com.myqnapcloud.mkokos.domain;

public enum ControlType {
    TOGGLEBUTTON,
    PUSHBUTTONON,
    PUSHBUTTONOFF,
    SLIDER,
    TEMPERATURE,
    TIMEPUSHBUTTONON,
    TIMEPUSHBUTTONOFF;
}
