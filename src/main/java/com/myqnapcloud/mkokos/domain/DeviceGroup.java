package com.myqnapcloud.mkokos.domain;//package com.myqnapcloud.mkokos.buttons.data;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

@Entity
@Table(name = "deviceIDgroup_tbl")
public class DeviceGroup {
    
    @Id
    @GeneratedValue(
            strategy = GenerationType.AUTO,
            generator = "native"
    )
    @GenericGenerator(
            name = "native",
            strategy = "native"
    )
    private Long id;
    private String shortName;
    @Column(unique = true, nullable = false)
    private String mac;
    private boolean active;
    @ManyToOne
    private User user;
    
    public boolean isActive() {
        return active;
    }
    
    public void setActive(boolean active) {
        this.active = active;
    }
    
    public Long getId() {
        return id;
    }
    
    public void setId(Long id) {
        this.id = id;
    }
    
    public User getUser() {
        return user;
    }
    
    public void setUser(User user) {
        this.user = user;
    }
    
    public String getMac() {
        return mac;
    }
    
    public void setMac(String mac) {
        this.mac = mac;
    }
    
    public DeviceGroup(String shortName, User user,String mac) {
        this.shortName = shortName;
        this.user = user;
        this.active = true;
        this.mac=mac;
    }
    
    public String getShortName() {
        return shortName;
    }
    
    public void setShortName(String shortName) {
        this.shortName = shortName;
    }
    
    public DeviceGroup() {
    }
    
    @Override
    public String toString() {
        return "DeviceID{" +
                "id=" + id +
                ", user=" + user +
                "}";
    }
}