package com.myqnapcloud.mkokos.domain;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

@Entity
@Table(name = "buttonconections_tbl")

public class ButtonConnection {

    @Id
    @GeneratedValue(
            strategy = GenerationType.AUTO,
            generator = "native"
    )
    @GenericGenerator(
            name = "native",
            strategy = "native"
    )
    private Long id;
    @ManyToOne
    private DeviceID buttonID;
    @ManyToOne
    private DeviceID relayID;
    private boolean active;

    public ButtonConnection(DeviceID buttonID, DeviceID relayID) {
        this.relayID = relayID;
        this.buttonID = buttonID;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public DeviceID getButtonID() {
        return buttonID;
    }

    public void setButtonID(DeviceID buttonID) {
        this.buttonID = buttonID;
    }

    public DeviceID getRelayID() {
        return relayID;
    }

    public void setRelayID(DeviceID relayID) {
        this.relayID = relayID;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }


    public ButtonConnection() {
    }

    @Override
    public String toString() {
        return "DeviceID{" +
                "id=" + id +
                ", button_name'" + buttonID.getShortName()
                + "'' description='" + buttonID.getDescription() + '\'' +
                " for relay='" + relayID.getShortName() +
                "}";
    }
}