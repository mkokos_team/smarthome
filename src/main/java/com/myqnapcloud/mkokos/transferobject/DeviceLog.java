package com.myqnapcloud.mkokos.transferobject;

import com.myqnapcloud.mkokos.domain.DeviceType;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class DeviceLog {
    private String name;
    private String description;
    private DeviceType type;
    private ArrayList<LogLine> logLines;

    public void addLog(LogLine logLine){

        this.logLines.add(logLine);
    }

    public DeviceLog(){
        this.logLines=new ArrayList<>();

    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public DeviceType getType() {
        return type;
    }

    public void setType(DeviceType type) {
        this.type = type;
    }

    public List<LogLine> getLogLines() {
        return logLines;
    }

    public void setLogLines(List<LogLine> logLines) {
        this.logLines =(ArrayList<LogLine>) logLines;

    }


    @Override
    public String toString() {
        return "DeviceLog{" +
                "name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", type=" + type +
                ", logLines=" + logLines +
                '}';
    }
}
