package com.myqnapcloud.mkokos.transferobject;

import java.time.LocalDateTime;

public class LogLine {
    private LocalDateTime dateTime;
    private String state;
    
    public LogLine() {
    }
    
    public LogLine(LocalDateTime dateTime, String state) {
        this.dateTime = dateTime;
        this.state = state;
    }
    
    public LocalDateTime getDateTime() {
        return dateTime;
    }
    
    public void setDateTime(LocalDateTime dateTime) {
        this.dateTime = dateTime;
    }
    
    public String getState() {
        return state;
    }
    
    public void setState(String state) {
        this.state = state;
    }
    
    @Override
    public String toString() {
        return "LogLine{" +
                "dateTime=" + dateTime +
                ", state='" + state + '\'' +
                "}\n";
    }
}
