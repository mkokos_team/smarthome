package com.myqnapcloud.mkokos.controllers;

import com.myqnapcloud.mkokos.domain.*;
import com.myqnapcloud.mkokos.repositories.*;
import com.myqnapcloud.mkokos.services.SecurityServiceImpl;
import com.myqnapcloud.mkokos.utilities.LoggingMy;
import com.myqnapcloud.mkokos.validator.DeviceValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import static com.myqnapcloud.mkokos.domain.DeviceType.BUTTON;
import static com.myqnapcloud.mkokos.domain.DeviceType.RELAY;

@RestController
public class DeviceIDControler {
    @Autowired
    private UserRepository userRepository;
    @Autowired
    DeviceIDRepository deviceIDRepository;
    @Autowired
    DeviceStateRepository deviceStateRepository;
    @Autowired
    StateRepository stateRepository;
    @Autowired
    ButtonConnectionsRepository buttonConnectionsRepository;
    @Autowired
    DeviceGroupRepository deviceGroupRepository;
    @Autowired
    private DeviceValidator deviceValidator;

    private static final Logger logger = LoggerFactory.getLogger(SecurityServiceImpl.class);


    @GetMapping("/api/{token}/device/state")
    public String showDeviceState(@PathVariable String token, @RequestParam String id) {
        DeviceState ds = deviceStateRepository.findFirstByDeviceIDOrderByDateTimeDesc(deviceIDRepository.findByUserAndIdAndActive(userRepository.findByToken(token), Long.parseLong(id), true));
        DeviceID di = deviceIDRepository.findById(Long.parseLong(id));
        return getState(ds, di);
    }

    static String getState(DeviceState ds, DeviceID di) {
        switch (di.getType()) {
            case RELAY:
                return ds.getBoolState();
            case BUTTON:
                return ds.getBoolState();
            case STEPPERMOTOR:
                return ds.getIntState();
            case TEMPERATURESENSOR:
                return ds.getFloatState();
            default:
                return "";
        }
    }


    //######################################################################################################################
    //Połaczenie
    @GetMapping("/api/{token}/connection/{buttonid}/add/{relayid}")
    public String addConection(@PathVariable("token") String token, @PathVariable("buttonid") String buttonid, @PathVariable("relayid") String relayid) {
        if (buttonConnectionsRepository.findByButtonIDAndRelayID(deviceIDRepository.findById(Long.parseLong(buttonid)), deviceIDRepository.findById(Long.parseLong(relayid))) != null) {
            return "exist";
        } else {
            ButtonConnection buttonConnection = new ButtonConnection(deviceIDRepository.findById(Long.parseLong(buttonid)), deviceIDRepository.findById(Long.parseLong(relayid)));
            buttonConnectionsRepository.save(buttonConnection);
            return "ok";
        }
    }

    @GetMapping("/api/{token}/connection/{buttonid}/delete/{relayid}")
    public String delConection(@PathVariable("token") String token, @PathVariable("buttonid") String buttonid, @PathVariable("relayid") String relayid) {
        if (buttonConnectionsRepository.findByButtonIDAndRelayID(deviceIDRepository.findById(Long.parseLong(buttonid)), deviceIDRepository.findById(Long.parseLong(relayid))) != null) {
            ButtonConnection buttonConnection = buttonConnectionsRepository.findByButtonIDAndRelayID(deviceIDRepository.findById(Long.parseLong(buttonid)), deviceIDRepository.findById(Long.parseLong(relayid)));
            buttonConnectionsRepository.delete(buttonConnection);
            return "exist";
        } else {
            logger.info(String.format("button %s relay %s ", buttonid, relayid));
            return "ok";
        }
    }

    //######################################################################################################################
    //Przycisk
    @GetMapping("/api/{token}/button")
    public List<DeviceID> showUsersButtons(@PathVariable("token") String token) {
        return deviceIDRepository.findByUserAndTypeAndActive(userRepository.findByToken(token), BUTTON, true);
    }

    @GetMapping("/api/{token}/button/{id}")
    public DeviceID showUsersButtonById(@PathVariable("token") String token, @PathVariable("id") String id) {
        return deviceIDRepository.findByUserAndIdAndActive(userRepository.findByToken(token), Long.parseLong(id), true);
    }

    @GetMapping("/api/{token}/button/{id}/TOGGLE")
    public String toggleUsersButtonById(@PathVariable("token") String token, @PathVariable("id") String id) {
        buttonConnectionsRepository.findByButtonID_Id(Long.parseLong(id)).stream().map(ButtonConnection::getRelayID).forEach(this::toogleBoolState);
        DeviceID deviceID = deviceIDRepository.findByUserAndIdAndActive(userRepository.findByToken(token), Long.parseLong(id), true);
        return toogleBoolState(deviceID);
    }

    @GetMapping("/api/{token}/button/{id}/STATE")
    public String getStateUsersButtonById(@PathVariable("token") String token, @PathVariable("id") String id) {
        DeviceID deviceID = deviceIDRepository.findByUserAndIdAndActive(userRepository.findByToken(token), Long.parseLong(id), true);
        DeviceState deviceState = deviceStateRepository.findFirstByDeviceIDOrderByDateTimeDesc(deviceID);
        return deviceState.getBoolState();
    }

    //######################################################################################################################
    //Przekaźnik
    @GetMapping("/api/{token}/relay")
    public List<DeviceID> showUsersRelay(@PathVariable("token") String token) {
        return deviceIDRepository.findByUserAndTypeAndActive(userRepository.findByToken(token), DeviceType.RELAY, true);
    }

    @GetMapping("/api/{token}/relay/{id}")
    public DeviceID showUsersRelayById(@PathVariable("token") String token, @PathVariable("id") String id) {
        return deviceIDRepository.findByUserAndIdAndActive(userRepository.findByToken(token), Long.parseLong(id), true);
    }

    @GetMapping("/api/{token}/relay/{id}/ON")
    public String setUsersRelayById(@PathVariable("token") String token, @PathVariable("id") String id, @RequestParam(required = false) String mstime) throws InterruptedException {
        DeviceID deviceID = deviceIDRepository.findByUserAndIdAndActive(userRepository.findByToken(token), Long.parseLong(id), true);
        DeviceState deviceState = new DeviceState(true, deviceID);
        stateRepository.save(deviceState.getState());
        deviceStateRepository.save(deviceState);
        if (mstime != null) {
            Long timeMs = Long.parseLong(mstime);
            timetogleRelayById(Long.parseLong(id), timeMs);
        }
        return "OK";
    }

    @Async
    public void timetogleRelayById(Long id, Long timeMs) throws InterruptedException {
        LoggingMy.consoleLog("przed opuznieniem");
        Thread.sleep(timeMs);
        LoggingMy.consoleLog("po opuznieniu");
        DeviceID deviceID = deviceIDRepository.findByIdAndActive(id, true);
        toogleBoolState(deviceID);
    }

    @GetMapping("/api/{token}/relay/{id}/OFF")
    public String resetUsersRelayById(@PathVariable("token") String token, @PathVariable("id") String id, @RequestParam(required = false) String mstime) throws InterruptedException {
        DeviceID deviceID = deviceIDRepository.findByUserAndIdAndActive(userRepository.findByToken(token), Long.parseLong(id), true);
        DeviceState deviceState = new DeviceState(false, deviceID);
        stateRepository.save(deviceState.getState());
        deviceStateRepository.save(deviceState);
        if (mstime != null) {
            Long timeMs = Long.parseLong(mstime);
            timetogleRelayById(Long.parseLong(id), timeMs);
        }
        return "OK";
    }

    @GetMapping("/api/{token}/relay/{id}/TOGGLE")
    public String toggleUsersRelayById(@PathVariable("token") String token, @PathVariable("id") String id) {
        DeviceID deviceID = deviceIDRepository.findByUserAndIdAndActive(userRepository.findByToken(token), Long.parseLong(id), true);
        return toogleBoolState(deviceID);
    }

    @GetMapping("/api/{token}/relay/{id}/STATE")
    public String getStateUsersRelayById(@PathVariable("token") String token, @PathVariable("id") String id) {
        DeviceID deviceID = deviceIDRepository.findByUserAndIdAndActive(userRepository.findByToken(token), Long.parseLong(id), true);
        DeviceState deviceState = deviceStateRepository.findFirstByDeviceIDOrderByDateTimeDesc(deviceID);
        return deviceState.getBoolState();
    }

    //######################################################################################################################
    //Temperatura
    @GetMapping("/api/{token}/temperature")
    public List<DeviceID> showUsersTemperature(@PathVariable("token") String token) {
        return deviceIDRepository.findByUserAndTypeAndActive(userRepository.findByToken(token), DeviceType.TEMPERATURESENSOR, true);
    }

    @GetMapping("/api/{token}/temperature/{id}/STATE")
    public String showUsersTemperatureById(@PathVariable("token") String token, @PathVariable("id") String id) {
        return deviceStateRepository.findFirstByDeviceIDOrderByDateTimeDesc(deviceIDRepository.findByUserAndIdAndActive(userRepository.findByToken(token), Long.parseLong(id), true)).getFloatState();
    }

    @GetMapping("/api/{token}/temperature/{id}/set/{temperature}")
    public String showUsersTemperatureById(@PathVariable("token") String token, @PathVariable("id") String id, @PathVariable("temperature") String temperature) {
        State state = new State(Float.parseFloat(temperature));
        DeviceState deviceState = new DeviceState(state, deviceIDRepository.findByUserAndIdAndActive(userRepository.findByToken(token), Long.parseLong(id), true));
        stateRepository.save(state);
        deviceStateRepository.save(deviceState);
        return "OK";
    }

    @GetMapping("/api/{token}/temperature/set")
    public String setTemperatureById(@PathVariable("token") String token, @RequestParam("id") String id, @RequestParam("temperature") String temperature) {
        State state = new State(Float.parseFloat(temperature));
        DeviceState deviceState = new DeviceState(state, deviceIDRepository.findByUserAndIdAndActive(userRepository.findByToken(token), Long.parseLong(id), true));
        stateRepository.save(state);
        deviceStateRepository.save(deviceState);
        return "OK";
    }

    //######################################################################################################################
    //silnik
    @GetMapping("/api/{token}/stepper")
    public List<DeviceID> showUsersSteper(@PathVariable("token") String token) {
        return deviceIDRepository.findByUserAndTypeAndActive(userRepository.findByToken(token), DeviceType.STEPPERMOTOR, true);
    }

    //######################################################################################################################
    //dodawanie uradzenia z poziomu mikrokontrolera - get z parametrami
    @RequestMapping("/api/{token}/add/relay")
    public String addRelay(@PathVariable("token") String token, @RequestParam(required = false) String shortname, @RequestParam(required = false) String description) {
        Date date = new Date();
        if (shortname == null) {
            shortname = "new_relay" + new Timestamp(date.getTime());
        }
        if (description == null) {
            description = "new_relay" + new Timestamp(date.getTime());
        }
        if (null != userRepository.findByToken(token)) {
            DeviceID deviceID = new DeviceID(shortname, description, DeviceType.RELAY, userRepository.findByToken(token));
            DeviceState deviceState = new DeviceState(false, deviceID);
            this.deviceIDRepository.save(deviceID);
            this.stateRepository.save(deviceState.getState());
            this.deviceStateRepository.save(deviceState);
            return deviceID.getId().toString();
        }
        return "error";
    }

    //######################################################################################################################
    //Krokowy
    @GetMapping("/api/{token}/add/steper")
    public String addSteper(@PathVariable("token") String token, @RequestParam(required = false) String shortname, @RequestParam(required = false) String description) {
        if (shortname == null) {
            shortname = "new_steper";
        }
        if (description == null) {
            Date date = new Date();
            description = "new_steper" + new Timestamp(date.getTime());
        }
        if (null != userRepository.findByToken(token)) {
            DeviceID deviceID = new DeviceID(shortname, description, DeviceType.STEPPERMOTOR, userRepository.findByToken(token));
            DeviceState deviceState = new DeviceState(0, 0, 100, deviceID);
            this.stateRepository.save(deviceState.getState());
            this.deviceIDRepository.save(deviceID);
            this.deviceStateRepository.save(deviceState);
            return deviceID.getId().toString();
        }
        return "error";
    }

    //######################################################################################################################
    //Termometr
    @GetMapping("/api/{token}/add/tempsens")
    public String addTempSens(@PathVariable("token") String token, @RequestParam(required = false) String shortname, @RequestParam(required = false) String description) {
        if (shortname == null) {
            shortname = "new_tempsens";
        }
        if (description == null) {
            Date date = new Date();
            description = "new_tempsens" + new Timestamp(date.getTime());
        }
        if (null != userRepository.findByToken(token)) {
            DeviceID deviceID = new DeviceID(shortname, description, DeviceType.TEMPERATURESENSOR, userRepository.findByToken(token));
            DeviceState deviceState = new DeviceState(0.0F, deviceID);
            this.stateRepository.save(deviceState.getState());
            this.deviceIDRepository.save(deviceID);
            this.deviceStateRepository.save(deviceState);
            return deviceID.getId().toString();
        }
        return "error";
    }

    //######################################################################################################################
    //Przycisk
    @GetMapping("/api/button/{id}/TOGGLE")
    public String toggleUsersButtonById(@PathVariable("id") String id) {
        DeviceID deviceID = deviceIDRepository.findByIdAndActive(Long.parseLong(id), true);
        return toogleBoolState(deviceID);
    }

    @GetMapping("/api/button/{id}/ON")
    public String setUsersButtonById(@PathVariable("id") String id) {
        DeviceID deviceID = deviceIDRepository.findByIdAndActive(Long.parseLong(id), true);
        DeviceState deviceState = new DeviceState(true, deviceID);
        stateRepository.save(deviceState.getState());
        deviceStateRepository.save(deviceState);
        return "OK";
    }

    @GetMapping("/api/button/{id}/OFF")
    public String resetUsersButtonById(@PathVariable("id") String id) {
        DeviceID deviceID = deviceIDRepository.findById(Long.parseLong(id));
        DeviceState deviceState = new DeviceState(false, deviceID);
        stateRepository.save(deviceState.getState());
        deviceStateRepository.save(deviceState);
        return "OK";
    }

    //######################################################################################################################
    //Przyciski i przekazniki
    @GetMapping({"/api/{token}/button4/{id}/TOGGLE/{mask}", "/api/{token}/button4/{id}/TOGGLE/{mask}", "/api/{token}/relay2/{id}/TOGGLE/{mask}", "/api/{token}/relay4/{id}/TOGGLE/{mask}"})
    public String toggleUsersMultiDevicesById(@PathVariable("token") String token, @PathVariable("id") String id, @PathVariable("mask") String mask) {
        List<DeviceID> deviceIDS = deviceIDRepository.findByDeviceGroupAndActive(deviceGroupRepository.findByUserAndIdAndActive(userRepository.findByToken(token), Long.parseLong(id), true), true);
        int i = 0;
        for (DeviceID deviceID : deviceIDS
        ) {
            if (mask.charAt(i) != '0') {

                toogleBoolState(deviceID);
                if (deviceID.getType() == BUTTON) {
                    for (ButtonConnection conection : buttonConnectionsRepository.findByButtonID_Id(deviceID.getId())) {
                        toogleBoolState(conection.getRelayID());
                    }

                }
            }
            i++;
        }
        return "ok";
    }

    @GetMapping("/api/{token}")
    public String showAll(@PathVariable("token") String token) {
        return deviceIDRepository.findByUserAndActive(userRepository.findByToken(token), true).toString();
    }

    private String toogleBoolState(DeviceID deviceID) {
        DeviceState lastDeviceState;
        lastDeviceState = deviceStateRepository.findFirstByDeviceIDOrderByDateTimeDesc(deviceID);
        DeviceState deviceState = new DeviceState(false, deviceID);
        if (lastDeviceState.getBoolState().equals("true")) {
            deviceState.setButtonState(false);
        } else {
            deviceState.setButtonState(true);
        }
        stateRepository.save(deviceState.getState());
        deviceStateRepository.save(deviceState);
        return lastDeviceState.getId().toString();
    }

    @GetMapping("/api/{token}/add/{mac}/{type}")
    public String newDevice(@PathVariable("token") String token, @PathVariable("mac") String mac, @PathVariable("type") String type, @RequestParam(required = false) String shortname, @RequestParam(required = false) String description) {
        DeviceID deviceID = deviceIDRepository.findByMac(mac);
        if (deviceID != null) {
            deviceID.setActive(true);
            deviceIDRepository.save(deviceID);
            LoggingMy.consoleLog("znaleziono mac w DeviceIDrepo");
            return deviceID.getId().toString();
        } else {
            DeviceGroup deviceGroup = deviceGroupRepository.findByMac(mac);
            if (deviceGroup != null) {
                deviceGroup.setActive(true);
                LoggingMy.consoleLog("znaleziono mac w DeviceGrouprepo");
                deviceGroupRepository.save(deviceGroup);
                List<DeviceID> deviceIDS = deviceIDRepository.findByDeviceGroupAndActive(deviceGroup, false);
                for (DeviceID d : deviceIDS
                ) {
                    d.setActive(true);
                    deviceIDRepository.save(d);
                }

                return deviceGroup.getId().toString();
            } else {
                LoggingMy.consoleLog("ne zanlezion mac");
                switch (type) {
                    case "relay":
                        return addDeviceMac(mac, Long.parseLong(addRelay(token, null, null)));
                    case "tempsens":
                        return addDeviceMac(mac, Long.parseLong(addTempSens(token, null, null)));
                    case "steper":
                        return addDeviceMac(mac, Long.parseLong(addSteper(token, null, null)));
                    case "relay4":
                        return addGroupMac(mac, Long.parseLong(addRelay4(token, mac, null, null)));
//                    case "relay2":
//                        return addGroupMac(mac, Long.parseLong(addRelay2(token, null, null)));
//                    case "button2":
//                        return addGroupMac(mac, Long.parseLong(addButton2(token, null, null)));
//                    case "button":
//                        return addGroupMac(mac, Long.parseLong(addButton(token, null, null)));
                    case "button4":
                        return addGroupMac(mac, Long.parseLong(addButton4(token, mac, null, null)));
                    default:
                        return "error";
                }
            }
        }
    }

    private String addDeviceMac(String mac, Long id) {
        DeviceID deviceID = deviceIDRepository.findById(id);
        deviceID.setMac(mac);
        deviceIDRepository.save(deviceID);
        return deviceID.getId().toString();
    }

    private String addGroupMac(String mac, Long id) {
        DeviceGroup deviceGroup = deviceGroupRepository.findById(id);
        deviceGroup.setMac(mac);
        deviceGroupRepository.save(deviceGroup);
        return deviceGroup.getId().toString();
    }

    @GetMapping("/api/{token}/relay4/{id}/STATE")
    public String getStateUsersRelay4ById(@PathVariable("token") String token, @PathVariable("id") String id) {
        return stateCheckInGroup(token, id);
    }

    @GetMapping("/api/{token}/relay2/{id}/STATE")
    public String getStateUsersRelay2ById(@PathVariable("token") String token, @PathVariable("id") String id) {
        return stateCheckInGroup(token, id);
    }

    private String stateCheckInGroup(String token, String id) {
        DeviceGroup deviceGroup = deviceGroupRepository.findByUserAndIdAndActive(userRepository.findByToken(token), Long.parseLong(id), true);
        List<DeviceID> deviceIDS = deviceIDRepository.findByDeviceGroupAndActive(deviceGroup, true);
        StringBuilder state = new StringBuilder();
        for (DeviceID deviceID : deviceIDS) {
            state.append((deviceStateRepository.findFirstByDeviceIDOrderByDateTimeDesc(deviceID).getBoolState().equals("true")) ? 1 : 0);
        }
        return state.toString();
    }

    //######################################################################################################################
    //dodawanie uradzenia z poziomu mikrokontrolera get z parametrami
    @RequestMapping("/api/{token}/add/relay4/{mac}")
    public String addRelay4(@PathVariable("token") String token, @PathVariable("mac") String mac, @RequestParam(required = false) String shortname, @RequestParam(required = false) String description) {
        if (shortname == null) {
            shortname = "new_relay4";
        }
        if (description == null) {
            Date date = new Date();
            description = "new_relay4" + new Timestamp(date.getTime());
        }
        return createNewDevices(shortname, token, 4, description, mac, RELAY);
    }

    private String createNewDevices(String shortname, String token, Integer count, String description, String mac, DeviceType deviceType) {
        if (null != userRepository.findByToken(token)) {
            DeviceGroup deviceGroup = new DeviceGroup(shortname, userRepository.findByToken(token), mac);
            deviceGroupRepository.save(deviceGroup);
            for (int i = 0; i < count; i++) {
                nameNewDevice(token, shortname, description, deviceType, deviceGroup, i);
            }
            return deviceGroup.getId().toString();
        }
        return null;
    }

    private void nameNewDevice(String token, String shortname, String description, DeviceType type, DeviceGroup deviceGroup, int i) {
        DeviceID deviceID = new DeviceID(shortname + "_" + i, description, type, userRepository.findByToken(token), deviceGroup);
        DeviceState deviceState = new DeviceState(false, deviceID);
        this.deviceIDRepository.save(deviceID);
        this.stateRepository.save(deviceState.getState());
        this.deviceStateRepository.save(deviceState);
    }

    //######################################################################################################################
    //Wyłączenie wszystkich urzadzeń - OK GOOGLE

    @GetMapping("/api/{token}/alloff")
    public String allOff() {
        List<DeviceID> deviceIDS = deviceIDRepository.findByTypeOrType(BUTTON, RELAY);
        for (DeviceID d : deviceIDS
        ) {
            DeviceState state = new DeviceState(false, d);
            stateRepository.save(state.getState());
            deviceStateRepository.save(state);

        }
        return "OK";
    }


    @GetMapping("/api/{token}/add/button4/{mac}")
    public String addButton4(@PathVariable("token") String token, @PathVariable("mac") String mac, @RequestParam(required = false) String shortname, @RequestParam(required = false) String description) {
        if (shortname == null) {
            shortname = "new_button4";
        }
        if (description == null) {
            Date date = new Date();
            description = "new_button4" + new Timestamp(date.getTime());
        }
        return createNewDevices(shortname, token, 4, description, mac, BUTTON);
    }

}
