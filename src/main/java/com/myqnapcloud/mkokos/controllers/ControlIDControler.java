package com.myqnapcloud.mkokos.controllers;

import com.myqnapcloud.mkokos.domain.ControlID;
import com.myqnapcloud.mkokos.repositories.ControlIDRepository;
import com.myqnapcloud.mkokos.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import static com.myqnapcloud.mkokos.utilities.LoggingMy.consoleLog;

@RestController
public class ControlIDControler {

    @Autowired
    private ControlIDRepository controlIDRepository;
    @Autowired
    private UserRepository userRepository;

    @GetMapping("/api/{token}/control/{id}/{pos}")
    public String setPosOfControl(@PathVariable("token") String token, @PathVariable("id") String id, @PathVariable("pos") String pos) {

        ControlID controlID = controlIDRepository.findByUserAndId(userRepository.findByToken(token), Long.parseLong(id));
        controlID.setPositionOfIcon(Integer.parseInt(pos));
        controlIDRepository.save(controlID);
        return "OK";
    }

    @GetMapping("/api/{token}/control/delete/{id}")
    public String deleteControl(@PathVariable("token") String token, @PathVariable("id") String id) {
        controlIDRepository.delete(controlIDRepository.findByUserAndId(userRepository.findByToken(token), Long.parseLong(id)));
        return "OK";
    }

    @GetMapping("/api/{token}/control/0/{pos}")
    public String setAddPosOfControl(@PathVariable("token") String token, @PathVariable("pos") String pos) {

        return "OK";
    }

    @GetMapping("/api/{token}/control/sort")
    public String sortPosOfControl(@PathVariable("token") String token, @RequestParam(value = "posArray[]") List<Integer> posArray) {
        consoleLog(posArray.toString());
        Integer i = 0;
        for (Integer id : posArray
        ) {
            if (id != 0) {
//                consoleLog("id konrtolki="+id.toString());
//                consoleLog("i="+i.toString());

                ControlID controlID = controlIDRepository.findById(id.longValue());
                controlID.setPositionOfIcon(i);
                controlIDRepository.save(controlID);
                i++;
            }

        }
        return "OK";
    }


}
