package com.myqnapcloud.mkokos.controllers;

import com.myqnapcloud.mkokos.domain.*;
import com.myqnapcloud.mkokos.repositories.*;
import com.myqnapcloud.mkokos.services.SecurityService;
import com.myqnapcloud.mkokos.services.UserService;
import com.myqnapcloud.mkokos.transferobject.DeviceLog;
import com.myqnapcloud.mkokos.transferobject.LogLine;
import com.myqnapcloud.mkokos.validator.ControlValidator;
import com.myqnapcloud.mkokos.validator.DeviceValidator;
import com.myqnapcloud.mkokos.validator.UserValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.ServletRequest;
import java.io.UnsupportedEncodingException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static com.myqnapcloud.mkokos.utilities.LoggingMy.consoleLog;

@Controller
public class UserController {
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private UserService userService;
    @Autowired
    private SecurityService securityService;
    @Autowired
    private ControlIDRepository controlIDRepository;
    @Autowired
    private ButtonConnectionsRepository buttonConnectionsRepository;
    @Autowired
    private DeviceIDRepository deviceIDRepository;
    @Autowired
    ServletRequest request;
    @Autowired
    private UserValidator userValidator;
    @Autowired
    private ControlValidator controlValidator;

    @Autowired
    private DeviceStateRepository deviceStateRepository;
    @Autowired
    DeviceGroupRepository deviceGroupRepository;
    @Autowired
    private DeviceValidator deviceValidator;



    private void standardModel(String token, String id, Model model) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String currentPrincipalName = authentication.getName();
        model.addAttribute("username", currentPrincipalName);
        model.addAttribute("device", deviceIDRepository.findByUserAndIdAndActive(userRepository.findByToken(token), Long.parseLong(id), true));
        List<DeviceID> rellayList = deviceIDRepository.findByUserAndTypeAndActive(userRepository.findByToken(token), DeviceType.RELAY, true);
        model.addAttribute("connections", buttonConnectionsRepository.findByButtonID_Id(Long.parseLong(id)));
        model.addAttribute("relays", rellayList);
    }
    @PostMapping("/{token}/edit/device/{id}")
    public String editDevice(@PathVariable("token") String token, @PathVariable("id") String id, @ModelAttribute("device") DeviceID deviceID, BindingResult bindingResult, Model model) {
        standardModel(token, id, model);
        deviceValidator.validate(deviceID, bindingResult);
        if (bindingResult.hasErrors()) {
            model.addAttribute("device", deviceIDRepository.findByUserAndIdAndActive(userRepository.findByToken(token), Long.parseLong(id), true));
            return "deviceform";
        } else {
            DeviceID oldDeviceID = deviceIDRepository.findById(deviceID.getId());
            deviceID.setType(oldDeviceID.getType());
            deviceID.setUser(oldDeviceID.getUser());
            deviceID.setActive(true);
            deviceIDRepository.save(deviceID);
            return "redirect:/devices";
        }
    }


    @GetMapping("/{token}/delete/device/{id}")
    public String deleteDevice(@PathVariable("token") String token, @PathVariable("id") String id, Model model) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String currentPrincipalName = authentication.getName();
        DeviceID deviceID = deviceIDRepository.findById(Long.parseLong(id));
        deviceID.setActive(false);
        DeviceGroup deviceGroup=deviceGroupRepository.findById(deviceID.getId());
        if (deviceGroup!=null){
            List <DeviceID> deviceIDS=deviceIDRepository.findByDeviceGroupAndActive(deviceGroup,true);
            if (deviceIDS==null){
                deviceGroup.setActive(false);
                deviceGroupRepository.save(deviceGroup);
            }
        }
        deviceIDRepository.save(deviceID);
        return "redirect:/devices";
    }

    @GetMapping("/{token}/edit/device/{id}")
    public String editDevice(@PathVariable("token") String token, @PathVariable("id") String id, Model model) {
        standardModel(token, id, model);
        return "deviceform";
    }
    @GetMapping("/registration")
    public String registration(Model model) {
        model.addAttribute("userForm", new User());
        return "registration";
    }

    @PostMapping("/registration")
    public String registration(@ModelAttribute("userForm") User userForm, BindingResult bindingResult) throws UnsupportedEncodingException {
        userValidator.validate(userForm, bindingResult);
        if (bindingResult.hasErrors()) {
            return "registration";
        }
        userForm.generateToken();
        userService.save(userForm);
        securityService.autoLogin(userForm.getUsername(), userForm.getPasswordConfirm());
        return "redirect:/";
    }

    @GetMapping("/login")
    public String login(Model model, String error, String logout) {
        if (error != null)
            model.addAttribute("error", "Twój login lub hasło nie są poprawne.");
        if (logout != null)
            model.addAttribute("message", "Zostałeś wylogowany.");
        return "login";
    }

    @GetMapping({"/devices"})
    public String devices(Model model) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String currentPrincipalName = authentication.getName();


        model.addAttribute("token", userRepository.findByUsername(currentPrincipalName).getToken());
        model.addAttribute("username", currentPrincipalName);
        model.addAttribute("apiDevice", deviceIDRepository.findByUserAndActive(userRepository.findByUsername(currentPrincipalName), true));
        return "devicelist";
    }

    @GetMapping({"/", "/welcome"})
    public String welcome(Model model) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String currentPrincipalName = authentication.getName();
        model.addAttribute("token", userRepository.findByUsername(currentPrincipalName).getToken());
        model.addAttribute("username", currentPrincipalName);
        model.addAttribute("apiControl", controlIDRepository.findByUserOrderByPositionOfIcon(userRepository.findByUsername(currentPrincipalName)));
        return "index";
    }






    @GetMapping("/chart/{id}")
    public String chartControl(Model model, @PathVariable("id") String id, @RequestParam(required = false) String stopDate, @RequestParam(required = false) String startDate) {
        getHistoryData(model, id, stopDate, startDate);
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String currentPrincipalName = authentication.getName();
        model.addAttribute("token", userRepository.findByUsername(currentPrincipalName).getToken());
        model.addAttribute("username", currentPrincipalName);
        return "chart";
    }

    @GetMapping("/log/{id}")
    public String loggingControl(Model model, @PathVariable("id") String id, @RequestParam(required = false) String stopDate, @RequestParam(required = false) String startDate) {
        getHistoryData(model, id, stopDate, startDate);
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String currentPrincipalName = authentication.getName();
        model.addAttribute("token", userRepository.findByUsername(currentPrincipalName).getToken());
        model.addAttribute("username", currentPrincipalName);
        return "log";
    }

    private void getHistoryData(Model model, String id, String stopDate, String startDate) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String currentPrincipalName = authentication.getName();
        model.addAttribute("username", currentPrincipalName);
        DeviceLog deviceLog;
        consoleLog("start date" + startDate);
        consoleLog("stop date" + stopDate);
        Date dateStart;
        Date dateStop;
        dateStart = Date.from(LocalDateTime.now().minusMonths(1).atZone(ZoneId.systemDefault()).toInstant());
        dateStop = Date.from(LocalDateTime.now().atZone(ZoneId.systemDefault()).toInstant());
        try {
            dateStart = new SimpleDateFormat("dd/MM/yyyy").parse(startDate);
            dateStop = new SimpleDateFormat("dd/MM/yyyy").parse(stopDate);
            dateStop.setTime(dateStop.getTime() + (1000 * 60 * 60 * 24) - 1);
        } catch (ParseException | NullPointerException e) {
            e.printStackTrace();
        }
        deviceLog = getLoggingData(deviceIDRepository.findById(Long.parseLong(id)), dateStart, dateStop);
        consoleLog("od: " + dateStart + " do: " + dateStop);
        consoleLog(deviceLog.toString());
        model.addAttribute("records", deviceLog);
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        String strStartDate = dateFormat.format(dateStart);
        String strStopDate = dateFormat.format(dateStop);
        model.addAttribute("viewStartDate", strStartDate);
        model.addAttribute("viewStopDate", strStopDate);
    }

    private DeviceLog getLoggingData(DeviceID deviceID, Date startDate, Date stopDate) {
        DeviceLog logData = new DeviceLog();
        logData.setName(deviceID.getShortName());
        logData.setDescription(deviceID.getDescription());
        logData.setType(deviceID.getType());
        List<DeviceState> deviceStates = deviceStateRepository.findByDeviceIDAndDateTimeBetween
                (deviceID,
                        startDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime(),
                        stopDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime());
        consoleLog(deviceStates.toString());
        List<LogLine> logLines = new ArrayList<>();
        for (DeviceState deviceState : deviceStates) {
            LogLine logLine = new LogLine();
            switch (deviceID.getType()) {
                case TEMPERATURESENSOR:
                    logLine.setDateTime(deviceState.getDateTime());
                    logLine.setState(deviceState.getFloatState());
                    break;
                case RELAY:
                case BUTTON:
                    logLine.setDateTime(deviceState.getDateTime());
                    logLine.setState(deviceState.getBoolState());
                    break;
                case STEPPERMOTOR:
                    logLine.setDateTime(deviceState.getDateTime());
                    logLine.setState(deviceState.getIntState());
                    break;
                default:
                    break;
            }
            logLines.add(logLine);
        }
        logData.setLogLines(logLines);
        return logData;
    }

    @GetMapping("/add")
    public String addControl(Model model) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String currentPrincipalName = authentication.getName();
        model.addAttribute("username", currentPrincipalName);
        ControlForm controlForm = new ControlForm();
        controlForm.setControltype("BUTTON");
        controlForm.setButtontype("SWICH");
        controlForm.setButtonstate("ON");
        model.addAttribute("addForm", new ControlForm());
        model.addAttribute("relays", deviceIDRepository.findByUserAndTypeAndActive(userRepository.findByUsername(currentPrincipalName), DeviceType.RELAY, true));
        model.addAttribute("temperatures", deviceIDRepository.findByUserAndTypeAndActive(userRepository.findByUsername(currentPrincipalName), DeviceType.TEMPERATURESENSOR, true));
        model.addAttribute("stepers", deviceIDRepository.findByUserAndTypeAndActive(userRepository.findByUsername(currentPrincipalName), DeviceType.STEPPERMOTOR, true));
        return "/add";

    }

    @PostMapping("/add")
    public String addControl(Model model, @ModelAttribute("addForm") ControlForm controlForm, BindingResult bindingResult) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String currentPrincipalName = authentication.getName();
        model.addAttribute("username", currentPrincipalName);
        model.addAttribute("token", userRepository.findByUsername(currentPrincipalName).getToken());
        controlValidator.validate(controlForm, bindingResult);
        if (bindingResult.hasErrors()) {
            model.addAttribute("relays", deviceIDRepository.findByUserAndTypeAndActive(userRepository.findByUsername(currentPrincipalName), DeviceType.RELAY, true));
            model.addAttribute("temperatures", deviceIDRepository.findByUserAndTypeAndActive(userRepository.findByUsername(currentPrincipalName), DeviceType.TEMPERATURESENSOR, true));
            model.addAttribute("stepers", deviceIDRepository.findByUserAndTypeAndActive(userRepository.findByUsername(currentPrincipalName), DeviceType.STEPPERMOTOR, true));
            return "add";
        } else {
            ControlID controlID = new ControlID();
            controlID.setShortName(controlForm.getShortName());
            controlID.setDescription(controlForm.getDescription());
            switch (controlForm.getControltype()) {
                case "BUTTON":
                    switch (controlForm.getButtontype()) {
                        case "PUSH":
                            if (controlForm.getButtonstate().equals("ON")) {
                                controlID.setType(ControlType.PUSHBUTTONON);
                            } else
                                controlID.setType(ControlType.PUSHBUTTONOFF);
                            break;
                        case "SWICH":
                            controlID.setType(ControlType.TOGGLEBUTTON);
                            break;
                        case "TIMER":
                            if (controlForm.getButtonstate().equals("ON")) {
                                controlID.setType(ControlType.TIMEPUSHBUTTONON);
                            } else {
                                controlID.setType(ControlType.TIMEPUSHBUTTONOFF);
                            }
                            controlID.setTimer(controlForm.getButtontimer());
                            break;
                    }
                    controlID.setDeviceID(deviceIDRepository.findById(Long.parseLong(controlForm.getDeviceIDRELAY())));
                    break;
                case "THERMOMETR":
                    controlID.setType(ControlType.TEMPERATURE);
                    controlID.setDeviceID(deviceIDRepository.findById(Long.parseLong(controlForm.getDeviceIDTEMPERATURE())));
                    break;
//                case "SLIDER":
//                    controlID.setType(ControlType.SLIDER);
//                    controlID.setDeviceID(deviceIDRepository.findById(Long.parseLong(controlForm.getDeviceIDSTEPER())));
//                    break;
            }
            controlID.setUser(userRepository.findByUsername(currentPrincipalName));
            controlIDRepository.save(controlID);
        }
        return "redirect:/";
    }
}
