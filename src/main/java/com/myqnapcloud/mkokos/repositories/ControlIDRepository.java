package com.myqnapcloud.mkokos.repositories;//package com.myqnapcloud.mkokos.buttons.data;

import com.myqnapcloud.mkokos.domain.ControlType;
import com.myqnapcloud.mkokos.domain.DeviceID;
import com.myqnapcloud.mkokos.domain.User;
import com.myqnapcloud.mkokos.domain.ControlID;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional
@EnableJpaRepositories(basePackages = "com.myqnapcloud.mkokos.domain", entityManagerFactoryRef = "entityManagerFactory")
public interface ControlIDRepository extends CrudRepository<ControlID, Integer> {

    ControlID findById(Long Id);
    ControlID findByshortName(String shortName);
    ControlID findByshortNameAndUser(String shortName,User user);
    ControlID findByUserAndId(User user, Long id);
    List<ControlID> findByUser(User user);
    List<ControlID> findByUserOrderByPositionOfIcon(User user);
    List<ControlID> findByUserAndType(User user,ControlType type);
    Long deleteByDeviceIDAndUser(DeviceID deviceID,User user);
    void deleteById(long id);
}