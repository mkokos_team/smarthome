package com.myqnapcloud.mkokos.repositories;//package com.myqnapcloud.mkokos.buttons.data;

import com.myqnapcloud.mkokos.domain.*;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional
@EnableJpaRepositories(basePackages = "com.myqnapcloud.mkokos.domain", entityManagerFactoryRef = "entityManagerFactory")
public interface ButtonConnectionsRepository extends CrudRepository<ButtonConnection, Integer> {

    List<ButtonConnection> findByButtonID_Id(Long id);
    ButtonConnection findByButtonIDAndRelayID(DeviceID buttonID, DeviceID RelayID);

}