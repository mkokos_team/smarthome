package com.myqnapcloud.mkokos.repositories;//package com.myqnapcloud.mkokos.buttons.data;

import com.myqnapcloud.mkokos.domain.DeviceGroup;
import com.myqnapcloud.mkokos.domain.User;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional
@EnableJpaRepositories(basePackages = "com.myqnapcloud.mkokos.domain", entityManagerFactoryRef = "entityManagerFactory")
public interface DeviceGroupRepository extends CrudRepository<DeviceGroup, Integer> {

    DeviceGroup findById(Long Id);
    void deleteByIdAndUser(Long id, User user);
    List<DeviceGroup> findByUserAndActive(User user, boolean active);
    DeviceGroup findByUserAndIdAndActive(User user, Long id, boolean active);
    DeviceGroup findByshortNameAndUserAndActive(String shortName, User user, boolean active);
    DeviceGroup findByIdAndActive(Long id, boolean active);
    DeviceGroup findByMac(String mac);
}