package com.myqnapcloud.mkokos.repositories;//package com.myqnapcloud.mkokos.buttons.data;

import com.myqnapcloud.mkokos.domain.DeviceID;
import com.myqnapcloud.mkokos.domain.DeviceGroup;
import com.myqnapcloud.mkokos.domain.DeviceType;
import com.myqnapcloud.mkokos.domain.User;
import com.myqnapcloud.mkokos.controllers.UserController;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional
@EnableJpaRepositories(basePackages = "com.myqnapcloud.mkokos.domain", entityManagerFactoryRef = "entityManagerFactory")
public interface DeviceIDRepository extends CrudRepository<DeviceID, Integer> {

    DeviceID findById(Long Id);
    DeviceID findByDescription(String description);
    void deleteByIdAndUser(Long id,User user);
    List<DeviceID> findByUserAndActive(User user,boolean active);
    List<DeviceID> findByTypeOrType(DeviceType type1,DeviceType type2);
    List<DeviceID> findByUserAndTypeAndActive(User user, DeviceType type,boolean active);
    DeviceID findByUserAndIdAndActive(User user,Long id,boolean active);
    DeviceID findByshortNameAndUserAndActive(String shortName,User user,boolean active);
    DeviceID findByIdAndActive(Long id,boolean active);
    List<DeviceID>findByDeviceGroupAndActive(DeviceGroup deviceGroup, boolean active);
    List<DeviceID>findByDeviceGroup(DeviceGroup deviceGroup);
    DeviceID findByMac(String mac);
    DeviceID findByMacAndType(String mac,String type);
}