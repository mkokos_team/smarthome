package com.myqnapcloud.mkokos.repositories;


import com.myqnapcloud.mkokos.domain.Role;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoleRepository extends JpaRepository<Role, Long>{
}
