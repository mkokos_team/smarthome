package com.myqnapcloud.mkokos.repositories;//package com.myqnapcloud.mkokos.buttons.data;

import com.myqnapcloud.mkokos.domain.DeviceID;
import com.myqnapcloud.mkokos.domain.DeviceState;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

@Transactional
@EnableJpaRepositories(basePackages = "com.myqnapcloud.mkokos.domains", entityManagerFactoryRef = "entityManagerFactory")
public interface DeviceStateRepository extends CrudRepository<DeviceState, Integer> {
    List<DeviceState> findBydateTimeAfter(LocalDateTime dateTime);
    List<DeviceState> findById(Long Id);
    List<DeviceState> findByIdAndDateTimeAfter(Long Id, LocalDateTime dateTime);
    DeviceState findFirstByIdOrderByIdDesc(Long Id);
    DeviceState findFirstById(Long Id);
    DeviceState findFirstByDeviceID(DeviceID deviceID);
    List<DeviceState> findByDeviceIDAndDateTimeBetween(DeviceID deviceID, LocalDateTime start,LocalDateTime stop);
    DeviceState findFirstByDeviceIDOrderByIdDesc(DeviceID deviceID);
    DeviceState findFirstByDeviceIDOrderByDateTimeDesc(DeviceID deviceID);
    List<DeviceState> findByDeviceID(DeviceID deviceID);
}