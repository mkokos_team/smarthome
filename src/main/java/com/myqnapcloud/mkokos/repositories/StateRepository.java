package com.myqnapcloud.mkokos.repositories;//package com.myqnapcloud.mkokos.buttons.data;

import com.myqnapcloud.mkokos.domain.DeviceID;
import com.myqnapcloud.mkokos.domain.State;
import com.myqnapcloud.mkokos.domain.State;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;

@Transactional
@EnableJpaRepositories(basePackages = "com.myqnapcloud.mkokos.domains", entityManagerFactoryRef = "entityManagerFactory")
public interface StateRepository extends CrudRepository<State, Integer> {
    List<State> findById(Long Id);
}