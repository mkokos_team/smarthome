package com.myqnapcloud.mkokos.repositories;


import com.myqnapcloud.mkokos.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface UserRepository extends JpaRepository<User, Long> {
    User findByUsername(String username);
    User findByToken(String token);
}

