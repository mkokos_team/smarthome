package com.myqnapcloud.mkokos.utilities;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class LoggingMy {
    private static boolean logging;

    public static void consoleLog(String info) {
        if (logging) {
            Logger logger = LoggerFactory.getLogger(LoggingMy.class);
            logger.info(info);
        }
    }

    public static boolean isLogging() {
        return logging;
    }

    public static void setLogging(boolean logging) {
        LoggingMy.logging = logging;
    }
}
