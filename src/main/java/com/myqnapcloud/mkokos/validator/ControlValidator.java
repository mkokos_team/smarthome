package com.myqnapcloud.mkokos.validator;


import com.myqnapcloud.mkokos.domain.ControlForm;
import com.myqnapcloud.mkokos.domain.ControlID;
import com.myqnapcloud.mkokos.domain.User;
import com.myqnapcloud.mkokos.repositories.ControlIDRepository;
import com.myqnapcloud.mkokos.repositories.UserRepository;
import com.myqnapcloud.mkokos.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

@Component
public class ControlValidator implements Validator {
    @Autowired
    private ControlIDRepository controlIDRepository;
    @Autowired
    private UserRepository userRepository;
    @Override
    public boolean supports(Class<?> aClass) {        return ControlForm.class.equals(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {
        ControlForm controlForm = (ControlForm) o;
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String currentPrincipalName = authentication.getName();
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "shortName", "NotEmpty");
        if (controlForm.getShortName().length() < 2 || controlForm.getShortName().length() > 16) {
            errors.rejectValue("shortName", "Size.userForm.username");
        }

    }
}
