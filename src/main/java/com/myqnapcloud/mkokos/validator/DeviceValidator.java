package com.myqnapcloud.mkokos.validator;


import com.myqnapcloud.mkokos.domain.ControlForm;
import com.myqnapcloud.mkokos.domain.DeviceID;
import com.myqnapcloud.mkokos.repositories.ControlIDRepository;
import com.myqnapcloud.mkokos.repositories.DeviceIDRepository;
import com.myqnapcloud.mkokos.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

@Component
public class DeviceValidator implements Validator {
    @Autowired
    private DeviceIDRepository deviceIDRepository;
    @Autowired
    private UserRepository userRepository;
    @Override
    public boolean supports(Class<?> aClass) {        return ControlForm.class.equals(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {
        DeviceID deviceID= (DeviceID) o;
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String currentPrincipalName = authentication.getName();
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "shortName", "NotEmpty");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "description", "NotEmpty");
        if (deviceID.getShortName().length() < 2 || deviceID.getShortName().length() > 16) {
            errors.rejectValue("shortName", "Size.userForm.username");
        }
        if (deviceIDRepository.findByshortNameAndUserAndActive(deviceID.getShortName(),userRepository.findByUsername(currentPrincipalName),true) != null) {
            errors.rejectValue("shortName", "Duplicate.descriptionForm.description");
        }

    }
}
