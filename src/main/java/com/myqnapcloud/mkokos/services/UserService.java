package com.myqnapcloud.mkokos.services;


import com.myqnapcloud.mkokos.domain.User;

public interface UserService {
    void save(User user);
    User findByUsername(String username);
}
