package com.myqnapcloud.mkokos;

import java.util.Collections;

import com.myqnapcloud.mkokos.utilities.LoggingMy;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.web.filter.CharacterEncodingFilter;

@EnableScheduling

@SpringBootApplication
@EnableAsync
public class SmarthomeApp {


    public static void main(String[] args) {
        LoggingMy.setLogging(true);
        SpringApplication app = new SpringApplication(SmarthomeApp.class);
        app.setDefaultProperties(Collections.singletonMap("server.servlet.context-path", ""));
        app.run(args);

    }



}