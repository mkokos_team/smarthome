<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<c:set var="contextPath" value="${pageContext.request.contextPath}"/>
<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Jekyll v3.8.5">
    <title>Smarthome</title>
    <link rel="canonical" href="https://getbootstrap.com/docs/4.3/examples/navbar-fixed/">
    <link href="../css/bootstrap.min.css" rel="stylesheet"
         " crossorigin="anonymous">
    <script src="https://kit.fontawesome.com/7e508416c3.js"></script>
    <style>
        .bd-placeholder-img {
            font-size: 1.125rem;
            text-anchor: middle;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
        }

        @media (min-width: 768px) {
            .bd-placeholder-img-lg {
                font-size: 3.5rem;
            }
        }
    </style>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="../js/packery.pkgd.min.js"></script>
    <script src="../js/draggabilly.pkgd.min.js"></script>
    <link href="../css/navbar-top-fixed.css" rel="stylesheet">
</head>
<body>
<%@include file="includes/menu.jsp" %>
<main role="main" class=" container wrapper">
    <table class="table">
        <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Nazwa</th>
            <th scope="col">Opis</th>
            <th scope="col">Grupa</th>
            <th scope="col">Typ</th>
            <th scope="col">Historia</th>
            <th scope="col">Edycja</th>
        </tr>
        </thead>
        <tbody>
        <c:forEach var="row" varStatus="loop" items="${apiDevice}">
            <tr>
                <th scope="row">${loop.index+1}</th>
                <td>${row.shortName}</td>
                <td>${row.description}</td>
                <td>${row.deviceGroup.shortName}</td>
                <td>
                    <c:choose>
                        <c:when test="${row.type eq 'BUTTON'}">
                            Przycisk
                        </c:when>
                        <c:when test="${row.type eq 'RELAY'}">
                            przekaźnik
                        </c:when>
                        <c:when test="${row.type eq 'TEMPERATURESENSOR'}">
                            termometr
                        </c:when>
                        <c:when test="${row.type eq 'STEPPERMOTOR'}">
                            silnik
                        </c:when>
                    </c:choose>
                </td>
                <td>
                    <a href="/log/${row.id}" style="margin-top: 64px ">Pokaż historie</a>
                </td>
                <td>
                    <a href="/${token}/edit/device/${row.id}">Edytuj</a>
                    &nbsp&nbsp<a href="/${token}/delete/device/${row.id}" class="text-danger">Usuń</a></td>
            </tr>
        </c:forEach>
        </tbody>
    </table>
</main>
<script src="../js/bootstrap.min.js"></script>
</body>
</html>


