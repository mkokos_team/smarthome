<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<c:set var="contextPath" value="${pageContext.request.contextPath}"/>

<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Jekyll v3.8.5">
    <title>Smarthome</title>
    <link rel="canonical" href="https://getbootstrap.com/docs/4.3/examples/navbar-fixed/">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link href="../css/bootstrap.min.css" rel="stylesheet"
           crossorigin="anonymous">
    <script src="https://kit.fontawesome.com/7e508416c3.js"></script>
    <style>
        .bd-placeholder-img {
            font-size: 1.125rem;
            text-anchor: middle;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
        }

        #droppable {
            position: absolute;
            bottom: 2%;
            right: 1%;
        }

        @media (min-width: 768px) {
            .bd-placeholder-img-lg {
                font-size: 3.5rem;
            }
        }
    </style>
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script>
        $(function () {
                $("#sortable").sortable({
                    update: function (event, ui) {
                        var posList = [];
                        $.each($(".grid-item"), function () {
                            // console.log($(this).attr("data-item-id"))

                            posList.push($(this).attr("data-item-id"))
                        })
                        // console.log(posList);
                        $.ajax({
                            type: "GET",
                            url: "/api/${token}/control/sort",
                            // contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
                            data: {
                                posArray: posList
                            },
                            success: function (response) {
                                // console.log("udało się");
                            },
                            error: function (e) {
                                alert('Error: ' + e);
                            }
                        });
                    }
                });
                $("#sortable").disableSelection();
                // $(".grid-item").draggable();
                $("#droppable").droppable({
                    accept: ".grid-item",
                    classes: {
                    },
                    drop: function (event, ui) {

                        if (confirm('Chcesz usunąć kontrolkę??')) {
                            var id = $(ui.draggable).data("item-id");
                            // console.log(id);
                            var url = "/api/${token}/control/delete/" + id;
                            $.get(url, function (data) {
                                // console.log(data);
                            });
                            $(ui.draggable).hide()
                        } else {
                            //
                        }

                    }
                });
            }
        );
    </script>
    <link href="../css/navbar-top-fixed.css" rel="stylesheet">
</head>
<body>
<%@include file="includes/menu.jsp" %>

<main role="main" class=" container wrapper">


    <div class="grid" id="sortable">
        <c:forEach var="row" items="${apiControl}">
            <c:if test="${row.deviceID.active eq true}">
                <c:choose>


                    <c:when test="${row.type eq 'TOGGLEBUTTON'}">

                        <div class="grid-item" data-item-id="${row.id}">
                            <div class="grid-item-inside ">
                                <table class="table-borderless align-items-center w-100 h-100">
                                    <tbody>
                                    <tr class="h-25">
                                        <td class="control-label">
                                                ${row.shortName}
                                        </td>
                                    </tr>
                                    <tr class="h-50 mx-auto">
                                        <td>

                                            <div class="onoffswitch">
                                                <input type="checkbox" name="onoffswitch"
                                                       class="onoffswitch-checkbox iotswich" id="${row.deviceID.id}"
                                                       checked>
                                                <label class="onoffswitch-label" for="${row.deviceID.id}">
                                                    <span class="onoffswitch-inner"></span>
                                                    <span class="onoffswitch-switch"></span>
                                                </label></div>


                                        </td>
                                    </tr>
                                    <tr class="h-25">
                                        <td>
                                            <a href="/log/${row.deviceID.id}" style="margin-top: 64px ">Wiecej>></a>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </c:when>

                    <c:when test="${row.type eq 'TEMPERATURE'}">
                        <div class="grid-item " data-item-id="${row.id}">
                            <div class="grid-item-inside">
                                <table class="table-borderless align-items-center w-100 h-100">
                                    <tbody>
                                    <tr class="h-25">
                                        <td class="control-label">
                                                ${row.shortName}
                                        </td>
                                    </tr>
                                    <tr class="h-50 mx-auto">
                                        <td><h2 class="iottemp" data-deviceid="${row.deviceID.id}">0.0</h2>
                                        </td>
                                    </tr>
                                    <tr class="h-25">
                                        <td><a href="/chart/${row.deviceID.id}">Wiecej>></a>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </c:when>
                    <c:when test="${row.type eq 'PUSHBUTTONON'}">
                        <div class="grid-item " data-item-id="${row.id}">
                            <div class="grid-item-inside">

                                <table class="table-borderless align-items-center w-100 h-100">
                                    <tbody>
                                    <tr class="h-25">
                                        <td class="control-label">
                                                ${row.shortName}
                                        </td>
                                    </tr>
                                    <tr class="h-50 mx-auto">
                                        <td>
                                            <a data-deviceid="${row.deviceID.id}" class="btn btn-success iotbuttonon"
                                               style="cursor: pointer"
                                            >ON</a>
                                        </td>
                                    </tr>
                                    <tr class="h-25">
                                        <td>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>

                        </div>
                    </c:when>
                    <c:when test="${row.type eq 'TIMEPUSHBUTTONON'}">
                        <div class="grid-item " data-item-id="${row.id}">
                            <div class="grid-item-inside">

                                <table class="table-borderless align-items-center w-100 h-100">
                                    <tbody>
                                    <tr class="h-25">
                                        <td class="control-label">
                                                ${row.shortName}
                                        </td>
                                    </tr>
                                    <tr class="h-50 mx-auto">
                                        <td>
                                            <a data-deviceid="${row.deviceID.id}" data-time="${row.timer}"
                                               class="btn btn-success iottimeron"
                                               style="cursor: pointer"
                                            >ON</a>
                                        </td>
                                    </tr>
                                    <tr class="h-25">
                                        <td>
                                            Na czas (${row.timer}ms)
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>

                        </div>
                    </c:when>
                    <c:when test="${row.type eq 'TIMEPUSHBUTTONOFF'}">
                        <div class="grid-item " data-item-id="${row.id}">
                            <div class="grid-item-inside">

                                <table class="table-borderless align-items-center w-100 h-100">
                                    <tbody>
                                    <tr class="h-25">
                                        <td class="control-label">
                                                ${row.shortName}
                                        </td>
                                    </tr>
                                    <tr class="h-50">
                                        <td><a data-deviceid="${row.deviceID.id}" data-time="${row.timer}"
                                               class="btn   mx-auto btn-danger iottimeroff"
                                               style="cursor: pointer"
                                        >OFF</a>
                                        </td>
                                    </tr>
                                    <tr class="h-25">
                                        <td>
                                            Na czas (${row.timer}ms)
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>

                        </div>
                    </c:when>
                    <c:when test="${row.type eq 'PUSHBUTTONOFF'}">
                        <div class="grid-item " data-item-id="${row.id}">
                            <div class="grid-item-inside">

                                <table class="table-borderless align-items-center w-100 h-100">
                                    <tbody>
                                    <tr class="h-25">
                                        <td class="control-label">
                                                ${row.shortName}
                                        </td>
                                    </tr>
                                    <tr class="h-50">
                                        <td><a data-deviceid="${row.deviceID.id}"
                                               class="btn  mx-auto btn-danger iotbuttonoff"
                                               style="cursor: pointer"
                                        >OFF</a>
                                        </td>
                                    </tr>
                                    <tr class="h-25">
                                        <td>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>

                        </div>
                    </c:when>

                    <c:otherwise>
                    </c:otherwise>
                </c:choose>
            </c:if>
        </c:forEach>


        <div class=" grid-item-invalid" data-item-id="0">
            <div class="grid-item-inside">

                <table class="table-borderless align-items-center w-100 h-100">
                    <tbody>
                    <tr class="h-25">
                        <td class="control-label">
                            Dodaj
                        </td>
                    </tr>
                    <tr class="h-50 mx-auto">
                        <td><a href="/add"> <i class="fas fa-plus-circle"
                                               style="margin-top:20px;font-size: 3rem"></i></a>
                        </td>
                    </tr>
                    <tr class="h-25">
                        <td>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <div id="droppable" class=" droppable grid-item trash-round" data-item-id="0" style="padding: 40px">


        <table class=" droppable table-borderless align-items-center w-100 h-100">
            <tbody>
            <tr class="droppable h-25">
                <td class="droppable control-label">
                    Usuń
                </td>
            </tr>
            <tr class="droppable h-50 mx-auto">
                <td><i class="droppable fas fa-trash-alt"
                       style="margin-top:20px;font-size: 3rem"></i>
                </td>
            </tr>
            <tr class="droppable h-25">
                <td>
                </td>
            </tr>
            </tbody>
        </table>


    </div>

    </div>


    <script>

        function checktemp() {
            $.each($('.iottemp'), function () {
                var id = $(this).data("deviceid");
                var temperature = "";
                // console.log(id);
                var url = "/api/${token}/device/state?id=" + id ;
                var widget = $(this);
                $.get(url, function (data) {
                    // console.log(data);
                    temperature = data;
                    // console.log(temperature);
                    widget.text(temperature);
                });
                // console.log(data);
            })
        }

        setInterval(checktemp, 600000);




        // });
        $('.iotswich').change(function () {
            // console.log(this.id);
            var url = "/api/${token}/relay/" + this.id + "/TOGGLE";
            $.get(url, function (data) {
                // console.log(data)


            });

        });
        $('.iotbuttonon').click(function () {
            var id = $(this).data("deviceid");
            // console.log(id);
            var url = "/api/${token}/relay/" + id + "/ON";
            $.get(url, function (data) {
                // console.log(data)


            }).done(function () {
                checkswich();
            });
        });
        $('.iottimeron').click(function () {
            var id = $(this).data("deviceid");
            var time = $(this).data("time");
            // console.log(id);
            var url = "/api/${token}/relay/" + id + "/ON?mstime=" + time;
            $.get(url, function (data) {
                // console.log(data)


            }).done(function () {
                checkswich();
            });
        });
        $('.iottimeroff').click(function () {
            var id = $(this).data("deviceid");
            var time = $(this).data("time");
            // console.log(id);
            var url = "/api/${token}/relay/" + id + "/OFF?mstime=" + time;
            $.get(url, function (data) {
                // console.log(data)


            }).done(function () {
                checkswich();
            });
        });
        $('.iotbuttonoff').click(function () {
            var id = $(this).data("deviceid");
            // console.log(id);
            var url = "/api/${token}/relay/" + id + "/OFF";
            $.get(url, function (data) {
                // console.log(data)


            }).done(function () {
                checkswich();
            });

        });

        function checkswich() {
            $.each($(".iotswich"), function (index) {
                // console.log( index + ": " + $( this ).id() );
                var url = "/api/${token}/device/state?id=" + this.id ;
                // console.log(url);
                var toogle = $(this);
                var state = $.ajax({type: "GET", url: "" + url, async: false}).responseText;
                //     $.get( url, function(data ) {
                //
                //   return data;
                // });
                // console.log(state);
                if (state == 'true') {
                    toogle.prop('checked', true);
                    // console.log("klikniety");
                } else {
                    toogle.removeAttr('checked');
                    // console.log("odkilkniety");
                }
            });
        }

        $(document).ready(function () {
            checkswich();
            checktemp();
        });

    </script>

</main>
<script src="../js/bootstrap.min.js"></script>
</body>
</html>


