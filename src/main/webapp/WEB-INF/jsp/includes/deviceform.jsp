<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<form:form method="POST" modelAttribute="device" class="form-horizontal">
    <h2 class="form-signin-heading">Edytuj urzadzenie</h2>
    <spring:bind path="shortName">
        <div class="form-group ${status.error ? 'has-error' : ''}">
            <label class="col-md-4 control-label" for="shortName">Nazwa</label>
            <form:input type="text" path="shortName" class="form-control" placeholder="Nazwa"
                        autofocus="true"></form:input>
            <form:errors path="shortName"></form:errors>
        </div>
    </spring:bind>
    <spring:bind path="description">
        <div class="form-group ${status.error ? 'has-error' : ''}">
            <label class="col-md-4 control-label" for="description">Opis</label>
            <form:textarea path="description" class="form-control" rows="4" cols="50"
                           placeholder="Opis"></form:textarea>
            <form:errors path="description"></form:errors>
        </div>
    </spring:bind>
    <spring:bind path="type">
        <div class="form-group ${status.error ? 'has-error' : ''}">
            <label class="col-md-4 control-label" for="type">Typ</label>
            <form:input disabled="true" path="type" class="form-control" rows="4" cols="50"
                        placeholder="Typ"></form:input>
            <form:errors path="type"></form:errors>
        </div>
    </spring:bind>
    <button class="btn btn-lg btn-primary btn-block" type="submit">Zmień</button>
    <br>
    <c:if test="${device.type eq'BUTTON'}">
        <fieldset class="rounded-lg border p-2 ">
            <legend class="w-auto">Podłączone przekazniki</legend>
            <table class="table">
                <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Nazwa</th>
                    <th scope="col">Opis</th>
                    <th scope="col">Edycja</th>
                </tr>
                </thead>
                <c:forEach var="row" varStatus="loop" items="${connections}">
                    <tr>
                        <th scope="row">${loop.index+1}</th>
                        <td>${row.relayID.shortName}</td>
                        <td>${row.relayID.description}</td>
                        <td><a href="/${token}/edit/device/${row.relayID.id}">Edytuj</a>&nbsp&nbsp<a href=""
                                                                                                     id="disconect"
                                                                                                     data-relayid="${row.relayID.id}"
                                                                                                     class="text-danger">Odłącz</a>
                        </td>
                    </tr>
                </c:forEach>
            </table>
            <div class="table table-borderless "></div>
        </fieldset>
        <fieldset class="rounded-lg border p-2 ">
            <legend class="w-auto">Dodaj przekaźnik</legend>
            <table>
                <tr>
                    <td colspan="2">
                        <label class=" control-label ">Wybierz przekaznik</label>
                    </td>
                </tr>
                <tr>
                    <td class="col-6"><select id="relay" class=" table form-control  ">
                        <c:forEach var="row" varStatus="loop" items="${relays}">
                            <option value="${row.id}">${row.shortName} ${row.description}</option>
                        </c:forEach>
                    </select></td>
                    <td class="col-4">
                        <button id="add" class=" btn btn-lg btn-success border-0" type="button">Dodaj przekaznik
                        </button>
                    </td>
                </tr>
            </table>
        </fieldset>
        <br>
    </c:if>
    <a href="/devices">
        <button class="btn btn-lg btn-danger btn-block" type="button">Anuluj</button>
    </a>
</form:form>
<script>$('#add').click(function () {
    var str = "";
    $("select option:selected").each(function () {
        str += $(this).val() + " ";
    });
    var url = "/api/${token}/connection/${device.id}/add/" + str;
    $.get(url, function (data) {
        console.log(data)
    }).done(function () {
        location.reload();
    });
});
$('#disconect').click(function () {
    var str = "";
    str += $(this).data("relayid");
    var url = "/api/${token}/connection/${device.id}/delete/" + str;
    $.get(url, function (data) {
        console.log(data)
    }).done(function () {
        location.reload();
    });
});
</script>
