<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<form:form method="POST" modelAttribute="addForm" class="form-horizontal">
    <h2 class="form-signin-heading">Dodaj kontrolkę</h2>
    <spring:bind path="shortName">
        <div class="form-group ${status.error ? 'has-error' : ''}">
            <label class="col-md-4 control-label" for="shortName">Nazwa</label>
            <form:input type="text" path="shortName" class="form-control" placeholder="Nazwa"
                        autofocus="true"></form:input>
            <form:errors path="shortName"></form:errors>
        </div>
    </spring:bind>
    <spring:bind path="description">
        <div class="form-group ${status.error ? 'has-error' : ''}">
            <label class="col-md-4 control-label" for="description">Opis</label>
            <form:textarea path="description" class="form-control" rows="4" cols="50"
                           placeholder="Opis"></form:textarea>
            <form:errors path="description"></form:errors>
        </div>
    </spring:bind>
    <label class=" control-label">Typ kontrolki</label>
    <spring:bind path="controltype">
        <div class="form-group ${status.error ? 'has-error' : ''}">
            <label class="radio-inline" for="BUTTON">
                <form:radiobutton id="BUTTON" path="controltype" value="BUTTON"/>Przycisk
            </label>
            <label class="radio-inline" for="THERMOMETR">
                <form:radiobutton id="THERMOMETR" path="controltype" value="THERMOMETR"/>
                Termometr</label>
            <form:errors path="controltype"></form:errors>
        </div>
    </spring:bind>
    <div class="form-group" id="setbuttontype" style="display: none">
        <label class="control-label">Typ przycisku</label>
        <spring:bind path="buttontype">
            <div class="form-group ${status.error ? 'has-error' : ''}">
                <label class=" control-label">
                    <form:radiobutton path="buttontype" id="push" checked="true" value="PUSH"/>Przyciskany</label>
                <label class=" control-label">
                    <form:radiobutton path="buttontype" id="toogle" value="SWICH"/>
                    Przełączany</label>
                <form:radiobutton path="buttontype" id="timer" value="TIMER"/>
                Czasowy</label>
                <form:errors path="buttontype"></form:errors>
            </div>
        </spring:bind>
    </div>
    <div class="form-group" id="sliderminmax" style="display: none">
        <spring:bind path="slidermin">
            <div class="form-group ${status.error ? 'has-error' : ''}">
                <label class="col-md-4 control-label" for="slidermin">Minimalna wartość</label>
                <form:input type="text" path="slidermin" class="form-control" placeholder="0"
                            autofocus="true"></form:input>
                <form:errors path="slidermin"></form:errors>
            </div>
        </spring:bind>
        <spring:bind path="slidermax">
            <div class="form-group ${status.error ? 'has-error' : ''}">
                <label class="col-md-4 control-label" for="slidermax">Maksymalna wartość</label>
                <form:input type="text" path="slidermax" class="form-control" placeholder="0"
                            autofocus="true"></form:input>
                <form:errors path="slidermax"></form:errors>
            </div>
        </spring:bind>
        <spring:bind path="sliderval">
            <div class="form-group ${status.error ? 'has-error' : ''}">
                <label class="col-md-4 control-label" for="sliderval">Początkowa wartość</label>
                <form:input type="text" path="sliderval" class="form-control" placeholder="0"
                            autofocus="true"></form:input>
                <form:errors path="sliderval"></form:errors>
            </div>
        </spring:bind>
    </div>
    <spring:bind path="buttonstate">
        <div class="form-group ${status.error ? 'has-error' : ''}" id="pushbutonstate" style="display: none">
            <label class="col-md-4 control-label" for="buttonstate">Własciwość przycisku</label>
            <form:select path="buttonstate" class="form-control">
                <form:option value="ON"/>
                <form:option value="OFF"/>
            </form:select>
            <form:errors path="buttonstate"></form:errors>
        </div>
    </spring:bind>
    <spring:bind path="buttontimer">
        <div class="form-group ${status.error ? 'has-error' : ''}" id="butontimer" style="display: none">
            <label class="col-md-4 control-label" for="buttontimer">Opóźnienie zmiany(ms)</label>
            <form:input type="text" path="buttontimer" class="form-control" placeholder="100"></form:input>
            <form:errors path="buttontimer"></form:errors>
        </div>
    </spring:bind>
    <spring:bind path="deviceIDRELAY">
        <div class="form-group ${status.error ? 'has-error' : ''}" id="devieceID-relay" style="display: none">
            <label class="col-md-4 control-label" for="buttonstate">Połączone urządzenie</label>
            <form:select path="deviceIDRELAY" class="form-control">
                <form:options items="${relays}" itemLabel="shortName" itemValue="id"/>
            </form:select>
            <form:errors path="buttonstate"></form:errors>
        </div>
    </spring:bind>
    <spring:bind path="deviceIDTEMPERATURE">
        <div class="form-group ${status.error ? 'has-error' : ''}" id="devieceID-temperature" style="display: none">
            <label class="col-md-4 control-label" for="buttonstate">Połączone urządzenie</label>
            <form:select path="deviceIDTEMPERATURE" class="form-control">
                <form:options items="${temperatures}" itemLabel="shortName" itemValue="id"/>
            </form:select>
            <form:errors path="buttonstate"></form:errors>
        </div>
    </spring:bind>
    <spring:bind path="deviceIDSTEPER">
        <div class="form-group ${status.error ? 'has-error' : ''}" id="devieceID-steper" style="display: none">
            <label class="col-md-4 control-label" for="buttonstate">Połączone urządzenie</label>
            <form:select path="deviceIDSTEPER" class="form-control">
                <form:options items="${stepers}" itemLabel="shortName" itemValue="id"/>
            </form:select>
            <form:errors path="buttonstate"></form:errors>
        </div>
    </spring:bind>
    <button class="btn btn-lg btn-primary btn-block" type="submit">Dodaj</button>
    <br><a href="/">
    <button class="btn btn-lg btn-danger btn-block" type="button">Anuluj</button>
</a>
</form:form>
<script>
    $("#BUTTON").click(function () {
        console.log("przycisk");
        $("#setbuttontype").show();
        $("#butontimer").hide();
        $("#sliderminmax").hide();
        $("#pushbutonstate").show();
        $("#sliderstate").hide();
        $("#devieceID-relay").show();
        $("#devieceID-temperature").hide();
        $("#devieceID-steper").hide();
    });
    $("#THERMOMETR").click(function () {
        console.log("temperatuire");
        $("#setbuttontype").hide();
        $("#sliderminmax").hide();
        $("#pushbutonstate").hide();
        $("#sliderstate").hide();
        $("#devieceID-relay").hide();
        $("#devieceID-temperature").show();
        $("#devieceID-steper").hide();
        $("#butontimer").hide();
    });
    $("#SLIDER").click(function () {
        console.log("slider");
        $("#setbuttontype").hide();
        $("#sliderminmax").show();
        $("#pushbutonstate").hide();
        $("#sliderstate").show();
        $("#devieceID-relay").hide();
        $("#devieceID-temperature").hide();
        $("#devieceID-steper").show();
        $("#butontimer").hide();

    });
    $("#push").click(function () {
        console.log("push");
        $("#setbuttontype").show();
        $("#sliderminmax").hide();
        $("#pushbutonstate").show();
        $("#sliderstate").hide();
        $("#butontimer").hide();
    });
    $("#toogle").click(function () {
        console.log("toogle");
        $("#setbuttontype").show();
        $("#sliderminmax").hide();
        $("#pushbutonstate").hide();
        $("#sliderstate").hide();
        $("#butontimer").hide();
    });
    $("#timer").click(function () {
        console.log("toogle");
        $("#setbuttontype").show();
        $("#sliderminmax").hide();
        $("#pushbutonstate").show();
        $("#sliderstate").hide();
        $("#butontimer").show();
    });
</script>