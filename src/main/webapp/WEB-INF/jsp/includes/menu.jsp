<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
    <a class="navbar-brand" style="cursor: default" href="#">Smarthome</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse"
            aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarCollapse">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item ">
                <a class="nav-link" href="/">Pulpit <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/devices">Lista Urządzeń <span class="sr-only">(current)</span></a>
            </li>
        </ul>
        <ul class="navbar-nav ">
            <%--            todo zmieniający sie kursor--%>
            <c:if test="${pageContext.request.userPrincipal.name != null}">
            <li class="nav-item nav-link text-white" style="cursor: default">Token:</li>
            <li><span class="nav-link text-success" style="cursor: default">${token}</span></li>
            <li class="nav-item">
                <form id="logoutForm" method="POST" action="${contextPath}/logout">
                    <input class="text-success" type="hidden" style="cursor: default" name="${_csrf.parameterName}"
                           value="${_csrf.token}"/>
                </form>
                <a class="nav-link " onclick="document.forms['logoutForm'].submit()" style="cursor: pointer">Wyloguj</a>
                </c:if>
            </li>
        </ul>
    </div>
    </div>
</nav>