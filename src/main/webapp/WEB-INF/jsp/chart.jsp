<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<c:set var="contextPath" value="${pageContext.request.contextPath}"/>
<%@ page import="com.google.gson.Gson" %>
<%@ page import="com.myqnapcloud.mkokos.transferobject.DeviceLog" %>
<%@ page import="com.myqnapcloud.mkokos.transferobject.LogLine" %>
<%@ page import="java.time.ZoneId" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.HashMap" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.Map" %>
<%
    Gson gsonObj = new Gson();
    Map<Object, Object> map = null;
    List<Map<Object, Object>> list = new ArrayList<Map<Object, Object>>();
    DeviceLog deviceLog = (DeviceLog) request.getAttribute("records");
    for (LogLine logLine : deviceLog.getLogLines()
    ) {
        map = new HashMap<Object, Object>();
        map.put("y", (float) Float.parseFloat(logLine.getState()));
        map.put("x", logLine.getDateTime().atZone(ZoneId.systemDefault()).toInstant().toEpochMilli());
        list.add(map);
    }
    String dataPoints = gsonObj.toJson(list);
%>
<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Jekyll v3.8.5">
    <title>Smarthome</title>
    <link rel="canonical" href="https://getbootstrap.com/docs/4.3/examples/navbar-fixed/">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <link href="../css/bootstrap.min.css" rel="stylesheet"
           crossorigin="anonymous">
    <style>
        .bd-placeholder-img {
            font-size: 1.125rem;
            text-anchor: middle;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
        }

        @media (min-width: 768px) {
            .bd-placeholder-img-lg {
                font-size: 3.5rem;
            }
        }
    </style>
    <script src="https://unpkg.com/bootstrap-datepicker@1.9.0/dist/js/bootstrap-datepicker.min.js"></script>
    <script src="https://unpkg.com/bootstrap-datepicker@1.9.0/dist/locales/bootstrap-datepicker.ar.min.js"
            charset="UTF-8"></script>
    <script src="https://unpkg.com/bootstrap-datepicker@1.9.0/dist/locales/bootstrap-datepicker.az.min.js"
            charset="UTF-8"></script>
    <script src="https://unpkg.com/bootstrap-datepicker@1.9.0/dist/locales/bootstrap-datepicker.bg.min.js"
            charset="UTF-8"></script>
    <script src="https://unpkg.com/bootstrap-datepicker@1.9.0/dist/locales/bootstrap-datepicker.bs.min.js"
            charset="UTF-8"></script>
    <script src="https://unpkg.com/bootstrap-datepicker@1.9.0/dist/locales/bootstrap-datepicker.ca.min.js"
            charset="UTF-8"></script>
    <script src="https://unpkg.com/bootstrap-datepicker@1.9.0/dist/locales/bootstrap-datepicker.cs.min.js"
            charset="UTF-8"></script>
    <script src="https://unpkg.com/bootstrap-datepicker@1.9.0/dist/locales/bootstrap-datepicker.cy.min.js"
            charset="UTF-8"></script>
    <script src="https://unpkg.com/bootstrap-datepicker@1.9.0/dist/locales/bootstrap-datepicker.da.min.js"
            charset="UTF-8"></script>
    <script src="https://unpkg.com/bootstrap-datepicker@1.9.0/dist/locales/bootstrap-datepicker.de.min.js"
            charset="UTF-8"></script>
    <script src="https://unpkg.com/bootstrap-datepicker@1.9.0/dist/locales/bootstrap-datepicker.el.min.js"
            charset="UTF-8"></script>
    <script src="https://unpkg.com/bootstrap-datepicker@1.9.0/dist/locales/bootstrap-datepicker.en-GB.min.js"
            charset="UTF-8"></script>
    <script src="https://unpkg.com/bootstrap-datepicker@1.9.0/dist/locales/bootstrap-datepicker.es.min.js"
            charset="UTF-8"></script>
    <script src="https://unpkg.com/bootstrap-datepicker@1.9.0/dist/locales/bootstrap-datepicker.et.min.js"
            charset="UTF-8"></script>
    <script src="https://unpkg.com/bootstrap-datepicker@1.9.0/dist/locales/bootstrap-datepicker.eu.min.js"
            charset="UTF-8"></script>
    <script src="https://unpkg.com/bootstrap-datepicker@1.9.0/dist/locales/bootstrap-datepicker.fa.min.js"
            charset="UTF-8"></script>
    <script src="https://unpkg.com/bootstrap-datepicker@1.9.0/dist/locales/bootstrap-datepicker.fi.min.js"
            charset="UTF-8"></script>
    <script src="https://unpkg.com/bootstrap-datepicker@1.9.0/dist/locales/bootstrap-datepicker.fo.min.js"
            charset="UTF-8"></script>
    <script src="https://unpkg.com/bootstrap-datepicker@1.9.0/dist/locales/bootstrap-datepicker.fr.min.js"
            charset="UTF-8"></script>
    <script src="https://unpkg.com/bootstrap-datepicker@1.9.0/dist/locales/bootstrap-datepicker.fr-CH.min.js"
            charset="UTF-8"></script>
    <script src="https://unpkg.com/bootstrap-datepicker@1.9.0/dist/locales/bootstrap-datepicker.gl.min.js"
            charset="UTF-8"></script>
    <script src="https://unpkg.com/bootstrap-datepicker@1.9.0/dist/locales/bootstrap-datepicker.he.min.js"
            charset="UTF-8"></script>
    <script src="https://unpkg.com/bootstrap-datepicker@1.9.0/dist/locales/bootstrap-datepicker.hr.min.js"
            charset="UTF-8"></script>
    <script src="https://unpkg.com/bootstrap-datepicker@1.9.0/dist/locales/bootstrap-datepicker.hu.min.js"
            charset="UTF-8"></script>
    <script src="https://unpkg.com/bootstrap-datepicker@1.9.0/dist/locales/bootstrap-datepicker.hy.min.js"
            charset="UTF-8"></script>
    <script src="https://unpkg.com/bootstrap-datepicker@1.9.0/dist/locales/bootstrap-datepicker.id.min.js"
            charset="UTF-8"></script>
    <script src="https://unpkg.com/bootstrap-datepicker@1.9.0/dist/locales/bootstrap-datepicker.is.min.js"
            charset="UTF-8"></script>
    <script src="https://unpkg.com/bootstrap-datepicker@1.9.0/dist/locales/bootstrap-datepicker.it.min.js"
            charset="UTF-8"></script>
    <script src="https://unpkg.com/bootstrap-datepicker@1.9.0/dist/locales/bootstrap-datepicker.it-CH.min.js"
            charset="UTF-8"></script>
    <script src="https://unpkg.com/bootstrap-datepicker@1.9.0/dist/locales/bootstrap-datepicker.ja.min.js"
            charset="UTF-8"></script>
    <script src="https://unpkg.com/bootstrap-datepicker@1.9.0/dist/locales/bootstrap-datepicker.ka.min.js"
            charset="UTF-8"></script>
    <script src="https://unpkg.com/bootstrap-datepicker@1.9.0/dist/locales/bootstrap-datepicker.km.min.js"
            charset="UTF-8"></script>
    <script src="https://unpkg.com/bootstrap-datepicker@1.9.0/dist/locales/bootstrap-datepicker.kk.min.js"
            charset="UTF-8"></script>
    <script src="https://unpkg.com/bootstrap-datepicker@1.9.0/dist/locales/bootstrap-datepicker.ko.min.js"
            charset="UTF-8"></script>
    <script src="https://unpkg.com/bootstrap-datepicker@1.9.0/dist/locales/bootstrap-datepicker.lt.min.js"
            charset="UTF-8"></script>
    <script src="https://unpkg.com/bootstrap-datepicker@1.9.0/dist/locales/bootstrap-datepicker.lv.min.js"
            charset="UTF-8"></script>
    <script src="https://unpkg.com/bootstrap-datepicker@1.9.0/dist/locales/bootstrap-datepicker.mk.min.js"
            charset="UTF-8"></script>
    <script src="https://unpkg.com/bootstrap-datepicker@1.9.0/dist/locales/bootstrap-datepicker.ms.min.js"
            charset="UTF-8"></script>
    <script src="https://unpkg.com/bootstrap-datepicker@1.9.0/dist/locales/bootstrap-datepicker.nl.min.js"
            charset="UTF-8"></script>
    <script src="https://unpkg.com/bootstrap-datepicker@1.9.0/dist/locales/bootstrap-datepicker.nl-BE.min.js"
            charset="UTF-8"></script>
    <script src="https://unpkg.com/bootstrap-datepicker@1.9.0/dist/locales/bootstrap-datepicker.no.min.js"
            charset="UTF-8"></script>
    <script src="https://unpkg.com/bootstrap-datepicker@1.9.0/dist/locales/bootstrap-datepicker.pl.min.js"
            charset="UTF-8"></script>
    <script src="https://unpkg.com/bootstrap-datepicker@1.9.0/dist/locales/bootstrap-datepicker.pt-BR.min.js"
            charset="UTF-8"></script>
    <script src="https://unpkg.com/bootstrap-datepicker@1.9.0/dist/locales/bootstrap-datepicker.pt.min.js"
            charset="UTF-8"></script>
    <script src="https://unpkg.com/bootstrap-datepicker@1.9.0/dist/locales/bootstrap-datepicker.ro.min.js"
            charset="UTF-8"></script>
    <script src="https://unpkg.com/bootstrap-datepicker@1.9.0/dist/locales/bootstrap-datepicker.sr.min.js"
            charset="UTF-8"></script>
    <script src="https://unpkg.com/bootstrap-datepicker@1.9.0/dist/locales/bootstrap-datepicker.sr-latin.min.js"
            charset="UTF-8"></script>
    <script src="https://unpkg.com/bootstrap-datepicker@1.9.0/dist/locales/bootstrap-datepicker.ru.min.js"
            charset="UTF-8"></script>
    <script src="https://unpkg.com/bootstrap-datepicker@1.9.0/dist/locales/bootstrap-datepicker.sk.min.js"
            charset="UTF-8"></script>
    <script src="https://unpkg.com/bootstrap-datepicker@1.9.0/dist/locales/bootstrap-datepicker.sl.min.js"
            charset="UTF-8"></script>
    <script src="https://unpkg.com/bootstrap-datepicker@1.9.0/dist/locales/bootstrap-datepicker.sq.min.js"
            charset="UTF-8"></script>
    <script src="https://unpkg.com/bootstrap-datepicker@1.9.0/dist/locales/bootstrap-datepicker.sr.min.js"
            charset="UTF-8"></script>
    <script src="https://unpkg.com/bootstrap-datepicker@1.9.0/dist/locales/bootstrap-datepicker.sr-latin.min.js"
            charset="UTF-8"></script>
    <script src="https://unpkg.com/bootstrap-datepicker@1.9.0/dist/locales/bootstrap-datepicker.sv.min.js"
            charset="UTF-8"></script>
    <script src="https://unpkg.com/bootstrap-datepicker@1.9.0/dist/locales/bootstrap-datepicker.sw.min.js"
            charset="UTF-8"></script>
    <script src="https://unpkg.com/bootstrap-datepicker@1.9.0/dist/locales/bootstrap-datepicker.th.min.js"
            charset="UTF-8"></script>
    <script src="https://unpkg.com/bootstrap-datepicker@1.9.0/dist/locales/bootstrap-datepicker.tr.min.js"
            charset="UTF-8"></script>
    <script src="https://unpkg.com/bootstrap-datepicker@1.9.0/dist/locales/bootstrap-datepicker.uk.min.js"
            charset="UTF-8"></script>
    <script src="https://unpkg.com/bootstrap-datepicker@1.9.0/dist/locales/bootstrap-datepicker.vi.min.js"
            charset="UTF-8"></script>
    <script src="https://unpkg.com/bootstrap-datepicker@1.9.0/dist/locales/bootstrap-datepicker.zh-CN.min.js"
            charset="UTF-8"></script>
    <script src="https://unpkg.com/bootstrap-datepicker@1.9.0/dist/locales/bootstrap-datepicker.zh-TW.min.js"
            charset="UTF-8"></script>
    <link id="bsdp-css" href="https://unpkg.com/bootstrap-datepicker@1.9.0/dist/css/bootstrap-datepicker3.min.css"
          rel="stylesheet">
    <link href="../css/bootstrap.min.css" rel="stylesheet">
    <link href="../css/navbar-top-fixed.css" rel="stylesheet">
    <script type="text/javascript">
        window.onload = function () {

            var chart = new CanvasJS.Chart("chartContainer", {
                theme: "light1", // "light2", "dark1", "dark2"
                animationEnabled: true,
                // zoomEnabled: true,
                title: {
                    text: "Temperatura czujnika: ${records.name}"
                },
                axisY: {
                    suffix: " °C"
                },
                axisX: {
                    valueFormatString: "H:mm D/M/YYYY"
                },
                data: [{
                    type: "line",
                    xValueType: "dateTime",
                    xValueFormatString: "H:mm D/M/YYYY",
                    yValueFormatString: "#0.0# °C",
                    dataPoints: <%out.print(dataPoints);%>
                }]
            });
            chart.render();

        }
    </script>
</head>
<body>
<%@include file="includes/menu.jsp" %>
<main role="main" class=" container wrapper">
    <span class="col-md-4">Nazwa: ${records.name}</span>
    <span class="col-md-6">Opis: ${records.description}</span>
    <span class="col-md-4"> Typ: ${records.type}</span>
    <span class="col-md-4"> Od: ${viewStartDate}</span>
    <span class="col-md-4"> Do: ${viewStopDate}</span>
    <br>
    <br>
    <form class="form-horizontal" method="GET">
        <%--todo forma przesyanie parametrów--%>
        <div class="input-daterange input-group" id="datepicker">
            <label style="padding-top: 0.5rem;" class="control-label input-group-addon">Wybierz zakres wyswietlania
                od&nbsp&nbsp</label>
            <input type="text" autocomplete="off" class="col-md-2 rounded-sm input-sm form-control" name="startDate"/>
            <span style="padding-top: 0.5rem;" class="input-group-addon">&nbsp&nbspto&nbsp&nbsp</span>
            <input type="text" autocomplete="off" class="col-md-2 rounded-sm input-sm form-control" name="stopDate"/>
            &nbsp&nbsp
            <button type="submit" class="rounded-sm btn-success">Pokaż</button>
        </div>
    </form>
    <br>
    <script>
        $(' #datepicker').datepicker({
            format: 'dd/mm/yyyy',
            startDate: '-1m',
            calendarWeeks: true
        });
    </script>
    <div id="chartContainer" style="height: 370px; width: 100%;"></div>
    <script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>
</main>
<script src="../js/bootstrap.min.js"></script>
</body>
</html>


